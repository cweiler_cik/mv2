<?php
//require_once('cikPdo.class.php');

function getModels( $year, $make )
{
	$dbh = new cikPdo;
	$dbh = $dbh->dbh;
	$q = "SELECT * FROM tdistinctvehicle WHERE Year = :year AND Make = :make";	

	try {

		$stmt = $dbh->prepare( $q );
		$stmt->bindParam( ':year', $year );
		$stmt->bindParam( ':make', $make );
		$stmt->execute();

	} catch ( PDOException $e ) {
		return $e->getMessage();
	}

	$result = $stmt->fetchAll( PDO::FETCH_ASSOC );

	if ( $result ):
		foreach ( $result as $row ) {
			$models[] = $row['Model'];
		}
	endif;

	return $models;
}

// http://hades.triauto.net/blp/lib/getMakes.php?year=1994
function getMakes( $year )
{
	$dbh = new cikPdo;
	$dbh = $dbh->dbh;
	$q = "SELECT DISTINCT Make FROM tdistinctvehicle WHERE Year = :year";

	try {
		$stmt = $dbh->prepare( $q );
		$stmt->bindParam( ':year', $year );
		$stmt->execute();

	} catch ( PDOException $e ) {
		return $e->getMessage();
	}

	$result = $stmt->fetchAll( PDO::FETCH_ASSOC );

	if ( $result ):
		foreach ( $result as $row ) {
			$makes[] = $row['Make'];
		}
	endif;

	return $makes;
}

function gatherSp( $params, $idStart )
{
	if ( !$idStart ){
		$idStart = 75100;
	}

	// $mysql = new mysqli('localhost', 'root', 'surface0', 'surface');
	$dbh = new cikPdo;
	$dbh = $dbh->dbh;
	$q = "SELECT * FROM salespersons WHERE SalepersonID > {$idStart} ORDER BY LastName";

	$c = 0;
	foreach ( $dbh->query( $q ) as $row ) {
	    $sp[$c]['fname'] = $row['FirstName'];
	    $sp[$c]['lname'] = $row['LastName'];
	    $sp[$c]['id']    = $row['SalepersonID'];
	    $c++;
	}

	$a = 0;
	foreach ($sp as $value) {
	    if ( $value['fname'] != '' && $value['fname'] != '' ):
	        $string = $value['lname'] . ', ' . $value['fname'];
	        $result['sp'][$a]['name'] = $string;
	        $result['sp'][$a]['id'] = $value['id'];
	        $a++;
	    endif;
	}

	// natcasesort( $result['sp'] );

	return $result['sp'];
}

function updateCustomerPrize( $params )
{
	$dbh = new cikPdo;
	$dbh = $dbh->dbh;

	$key 		= $params['customer-key'];
	$number 	= $params['prizeClaimNumber'];
	$entered 	= $params['prizeClaimNumberEntered'];
	$desc 		= $params['prizeDesc'];
	$job 		= $params['job'];
	$endTime 	= mktime( date("H"), date("i"), date("s"), date("m"), date("d"), date("Y") );

	if ( $params['subjobs_no'] ) {
		$subjob = $params['subjobs_no'];
	} else {
		$subjob = 1;
	}

	$q = "INSERT INTO tcustomerprize (
		`CustomerKey`,
		`PrizeClaimNumber`,
		`PrizeClaimNumberEntered`,
		`PrizeDescription`,
		`jobs_no`,
		`subjobs_no`,
		`entry_user`,
		`entry_timestamp`,
		`entry_email`,
		`DefaultPrize`
	) VALUES (
		:cusKey,
		:winnerNumber,
		:playerNumber,
		:prizeDesc,
		:jobs_no,
		:subjobs_no,
		'13',
		:endTime,
		'F',
		'F'
	)";
	
	try {	
		$stmt = $dbh->prepare( $q );
		$stmt->bindParam( ':cusKey', $key );
		$stmt->bindParam( ':winnerNumber', $number );
		$stmt->bindParam( ':playerNumber', $entered );
		$stmt->bindParam( ':prizeDesc', $desc );
		$stmt->bindParam( ':jobs_no', $job );
		$stmt->bindParam( ':subjobs_no', $subjob );
		$stmt->bindParam( ':endTime', $endTime );

		$result = $stmt->execute();
	} catch ( PDOException $e ) {
		return $e->getMessage();
	}

	if ( $stmt->rowCount() > 0 ) {
		return json_encode(array( 'success' => true, 'message' => $stmt->rowCount() . ' rows affected' ));
	} else {
		return false;
	}
}

function updatePrizeLog( $params )
{
	$dbh 			= new cikPdo;
	$dbh 			= $dbh->dbh;

	$key 			= $params['customer-key'];
	$job 			= $params['job'];
	$whichPlay 		= $params['whichPlay'];
	$odds 			= $params['odds'];
	$playOdds 		= $params['playOdds'];
	$playerNumber 	= $params['playerNumber'];
	$winnerNumber 	= $params['winnerNumber'];
	$endTime 		= mktime( date("H"), date("i"), date("s"), date("m"), date("d"), date("Y") );

	if ( $params['subjob'] ) {
		$subjob	= $params['subjob'];
	} else {
		$subjob = 1;
	}

	if ( $params['user_agent'] != 'ie' ):
		header( 'Content-type: application/json' );
	else:
		header( 'Content-type: text/plain' );
	endif;

	$q = "INSERT INTO tprizelog (
		`CustomerKey`,
		`jobs_no`,
		`subjobs_no`,
		`whichPlay`,
		`odds`,
		`playOdds`,
		`playerNumber`,
		`winnerNumber`,
		`ts`
	) VALUES (
		:cusKey,
		:jobs_no,
		:subjobs_no,
		:whichPlay,
		:odds[$whichPlay],
		:playOdds,
		:playerNumber,
		:winnerNumber,
		:endTime
	)";

	try {
		$stmt = $dbh->prepare( $q );
		$stmt->bindParam( ':cusKey', $key );
		$stmt->bindParam( ':jobs_no', $job );
		$stmt->bindParam( ':subjobs_no', $subjob );
		$stmt->bindParam( ':whichPlay', $whichPlay );
		$stmt->bindParam( ':odds', $odds );
		$stmt->bindParam( ':playOdds', $playOdds );
		$stmt->bindParam( ':playerNumber', $playerNumber );
		$stmt->bindParam( ':winnerNumber', $winnerNumber );
		$stmt->bindParam( ':endTime', $endTime );

		$result = $stmt->execute();
	} catch ( PDOException $e ) {
		return $e->getMessage();
	}

	if ( $stmt->rowCount() > 0 ) {
		// return true;
		return json_encode(array( 'success' => true, 'message' => $stmt->rowCount() . ' rows affected' ));
	} else {
		return false;
		// exit( json_encode(array( 'success' => false, 'message' => 'No rows affected' )) );
	}
}

function updatePrizes( $params )
{
	$prize 	= updateCustomerPrize( $params );
	$log 	= updatePrizeLog( $params );
	
	$result = array(
		'updateCustomerPrize'	=> $prize,
		'updateLog'				=> $log
		);

	return $result;
}

function updateCustomer( $params )
{
	$dbh = new cikPdo;
	$dbh = $dbh->dbh;

	$job				= $params['job'];
	$scanned 			= 'T';
	$key				= $params['customer-key'];
	$fname 				= $params['edit-customer-fname']; // $params['customer-fname'];
	$lname 				= $params['edit-customer-lname']; // $params['customer-lname'];
	$address 			= $params['edit-customer-address']; // $params['cutsomer-address'];
	$city 				= $params['city']; // $params['customer-city'];
	$state 				= $params['state']; // $params['customer-state'];
	$zip 				= $params['zip']; // $params['customer-zip'];
	$email 				= $params['email']; // $params['customer-email'];
	$year 				= $params['year'];
	$make 				= $params['make'];
	$model 				= $params['model'];
	$mileage			= $params['mileage'];
	$yearMakeModel 		= $year . ' ' . $make . ' ' . $model;
	$phone 				= $params['phone'];
	$cell 				= $params['cell'];
	// $scanDate 		= $params['scanDate'];
	$scanDate 			= date( 'm-d-Y', time() );
	$start 				= $params['startTime'];
	$finish 			= mktime( date("H"), date("i"), date("s"), date("m"), date("d"), date("Y") );
	$spId 				= $params['spLname'] . ', ' . $params['spFname'];
	$test 				= 'F';
	$followUp 			= 'F';

	$params['scanDate'] = $scanDate;

	if ($params['subjob']):
		$subjob = $params['subjob'];
	else:
		$subjob = 1;
	endif;

	if ($params['drop']){
		$drops_no = $params['drop'];
	} else {
		$drops_no = 1;
	}

	$q = "UPDATE tcustomer SET
			`Scanned` 					= :scanned,
			`CustomerFirstNameChange` 	= :fname,
			`CustomerLastNameChange` 	= :lname,
			`CustomerAddressChange`		= :address,
			`CustomerCityChange` 		= :city,
			`CustomerStateChange`		= :state,
			`CustomerZIPChange` 		= :zip,
			`CustomerYearMakeModel` 	= :yearMakeModel,
			`CustomerEmail` 			= :email,
			`CustomerPhone` 			= :phone,
			`CustomerCell` 				= :cell,
			`ScanDate` 					= :scanDate,
			`StartTime` 				= :start,
			`EndTime` 					= :finish,
			`SalespersonID` 			= :spId,
			`TestRecord` 				= :test,
			`FollowUp` 					= :followUp
		WHERE `CustomerKey` = :key
		AND `jobs_no` 		= :job";

	try {
		$stmt = $dbh->prepare( $q );

		$stmt->bindParam( ':key', $key );
		$stmt->bindParam( ':job', $job );
		$stmt->bindParam( ':scanned', $scanned );
		$stmt->bindParam( ':fname', $fname );
		$stmt->bindParam( ':lname', $lname );
		$stmt->bindParam( ':address', $address );
		$stmt->bindParam( ':city', $city );
		$stmt->bindParam( ':state', $state );
		$stmt->bindParam( ':zip', $zip );
		$stmt->bindParam( ':yearMakeModel', $yearMakeModel );
		$stmt->bindParam( ':email', $email );
		$stmt->bindParam( ':phone', $phone );
		$stmt->bindParam( ':cell', $cell );
		$stmt->bindParam( ':scanDate', $scanDate );
		$stmt->bindParam( ':start', $start );
		$stmt->bindParam( ':finish', $finish );
		$stmt->bindParam( ':spId', $spId );
		$stmt->bindParam( ':test', $test );
		$stmt->bindParam( ':followUp', $followUp );	
		
		$result = $stmt->execute();
	} catch ( PDOException $e ) {
		return json_encode($e->getMessage());
	}

	if ( $params['user_agent'] != 'ie' ):
		header( 'Content-type: application/json' );
	else:
		header( 'Content-type: text/plain' );
	endif;

	if ( $stmt->rowCount() > 0 ) {
		// return true;
		return json_encode(array( 'success' => true, 'message' => $stmt->rowCount() . ' rows affected' ));
	} else {
		// return false;
		return json_encode(array(
			'success' 	=> false,
			'message' 	=> 'No rows affected',
			'debug'		=>  $result
			)
		);
	}
}

function createCustomer( $params )
{
	// http://hades.triauto.net/mv/lib/createCustomer.php?job=81080&drop=1&sp=75139&customer-key=81080000025&customer-fname=Test&customer-lname=Testerson&customer-address=555 Somewhere St&customer-city=Indianapolis&customer-state=IN&customer-zip=46227&year=2004&make=SATURN&model=ION&mileage=70000&customer-email=testcustomer@tester.com&phone=3177024504&cell=3177024504&startTime=1368536747219&endTime=1368536808331
	$dbh 			= new cikPdo;
	$dbh			= $dbh->dbh;

	$job			= $params['job'];
	$drops_no		= $params['drop'];
	$scanned 		= 'T';
	$uploaded 		= 0;
	$key			= $params['customer-key'];
	$fname 			= $params['customer-fname'];
	$lname 			= $params['customer-lname'];
	$address 		= $params['cutsomer-address'];
	$city 			= $params['customer-city'];
	$state 			= $params['customer-state'];
	$zip 			= $params['customer-zip'];
	$email 			= $params['customer-email'];
	$year 			= $params['year'];
	$make 			= $params['make'];
	$model 			= $params['model'];
	$mileage		= $params['mileage'];
	$yearMakeModel 	= $year . ' ' . $make . ' ' . $model;
	$phone 			= $params['phone'];
	$cell 			= $params['cell'];
	$scanDate 		= $params['scanDate'];
	$start 			= $params['startTime'];
	$finish 		= mktime( date("H"), date("i"), date("s"), date("m"), date("d"), date("Y") );
	$spId 			= $params['sp'];
	$test 			= 'T';
	$followUp 		= 'T';

	if ($params['subjob']):
		$subjob = $params['subjob'];
	else:
		$subjob = 1;
	endif;

	$q = "INSERT INTO tcustomer (
			CustomerKey,
			jobs_no,
			subjobs_no,
			drops_no,
			Scanned,
			Uploaded,
			CustomerFirstName,
			CustomerLastName,
			CustomerAddress,
			CustomerCity,
			CustomerState,
			CustomerZip,
			CustomerYearMakeModel,
			CustomerEmail,
			CustomerPhone,
			CustomerCell,
			ScanDate,
			StartTime,
			EndTime,
			SalespersonID,
			TestRecord,
			FollowUp
		) VALUES (
			:key,
			:job,
			:subjob,
			:drops_no,
			:scanned,
			:uploaded,
			:fname,
			:lname,
			:address,
			:city,
			:state,
			:zip,
			:yearMakeModel,
			:email,
			:phone,
			:cell,
			:scanDate,
			:start,
			:finish,
			:spId,
			:test,
			:followUp
		)";

	try {		
		$stmt = $dbh->prepare( $q );
		$stmt->bindParam( ':key', $key );
		$stmt->bindParam( ':job', $job );
		$stmt->bindParam( ':subjob', $subjob );
		$stmt->bindParam( ':drops_no', $drops_no );
		$stmt->bindParam( ':scanned', $scanned );
		$stmt->bindParam( ':uploaded', $uploaded );
		$stmt->bindParam( ':fname', $fname );
		$stmt->bindParam( ':lname', $lname );
		$stmt->bindParam( ':address', $address );
		$stmt->bindParam( ':city', $city );
		$stmt->bindParam( ':state', $state );
		$stmt->bindParam( ':zip', $zip );
		$stmt->bindParam( ':yearMakeModel', $yearMakeModel );
		$stmt->bindParam( ':email', $email );
		$stmt->bindParam( ':phone', $phone );
		$stmt->bindParam( ':cell', $cell );
		$stmt->bindParam( ':scanDate', $scanDate );
		$stmt->bindParam( ':start', $start );
		$stmt->bindParam( ':finish', $finish );
		$stmt->bindParam( ':spId', $spId );
		$stmt->bindParam( ':test', $test );
		$stmt->bindParam( ':followUp', $followUp );
		$stmt->execute();

	} catch ( PDOException $e ) {
		return json_encode($e->getMessage());
	}

	if ( $stmt->rowCount() > 0 ) {
		// return true;
		return json_encode(array( 'success' => true, 'message' => $stmt->rowCount() . ' rows affected' ));
	} else {
		return false;
		// exit( json_encode(array( 'success' => false, 'message' => 'No rows affected' )) );
	}
	// exit( json_encode(array('success' => 'true')) );
}