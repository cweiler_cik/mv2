<?php

// http://hades.triauto.net/mv/lib/selectCustomer.php?job=81080&customer-key=81080000025
require_once( 'functions.php' );

$dbh 	= new cikPdo;
$dbh 	= $dbh->dbh;
$params = $_REQUEST;

$job	= $params['job'];
$key	= $params['customer-key'];

$query = "SELECT * FROM `tcustomer` WHERE `jobs_no` = :job AND `CustomerKey` = :key";

$sel = $dbh->prepare( $query );
$sel->bindParam( ':job', $job );
$sel->bindParam( ':key', $key );
$sel->execute();

$result = $sel->fetchAll( PDO::FETCH_ASSOC );

if ( $result ):
	foreach ( $result as $row ){
		$data['result'] = $row;
	}
	exit( json_encode( $data ) );
else:
	exit( json_encode( array( 'success' => false ) ) );
endif;