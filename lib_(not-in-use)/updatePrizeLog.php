<?php

// http://hades.triauto.net/mv/lib/updatePrizeLog.php?
require_once( 'functions.php' );

$dbh 	= new cikPdo;
$dbh 	= $dbh->dbh;
$params	= $_REQUEST;

$result = updatePrizeLog( $params );
$json 	= json_encode( $result );

if ( $params['user_agent'] != 'ie' ):
	header( 'Content-type: application/json' );
else:
	header( 'Content-type: text/plain' );
endif;

exit( $json );