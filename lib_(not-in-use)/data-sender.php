<?php
$key = "##ghKl7rt3z9";
$mysql = new Mysqli( 'localhost', 'root', 'surface0', 'surface' );
function cleanString($string, $chars = 0, $with = '') {
  $string = "A" . $string;

  if (empty($chars)) {
    $chars = array('"','\'','\\','&');
  }

  for ($i = 0; $i < count($chars); $i++) {
    while (strpos($string, $chars[$i]) != 0) {
      $index = strpos($string, $chars[$i]);
      $string = substr($string, 0, $index) . $with . substr($string, $index+1);
    }
  }

  $string = substr($string,1);
  return $string;
}

// WILL ALWAYS NEED TO INSERT SALESPERSONS - HOW DO I KNOW BARCODE?
$selectSales = "select * from salespersons where Uploaded = 'F'";
$resultSales = $mysql->query($selectSales);
if ($resultSales->num_rows > 0) {
	while ($rowSales = $resultSales->fetch_array()) {
		$sql[] = "insert into salespersons (FirstName,LastName,Email,Clear) VALUES ('" . addslashes(cleanString($rowSales[FirstName])) . "', '" . addslashes(cleanString($rowSales[LastName])) . "', '" . addslashes(cleanString($rowSales[Email])) . "', 'F')";
		$updateSale = "update salespersons set Uploaded = 'T' where Barcode = '" . $rowSales[Barcode] . "'";
		$resultSale = $mysql->query($updateSale);	
	}
}

$selectCus = "select * from tcustomer where Scanned = 'T' AND Uploaded = 'F'";
$resultCus = $mysql->query($selectCus);
if ($resultCus->num_rows > 0) {
	while ($rowCus = $resultCus->fetch_array()) {
		$sql[] = "|select * from tcustomer where SerialKey='" . $rowCus[SerialKey] . "'";
		$sql[] = "|update tcustomer set CustomerFirstNameChange = '" . addslashes(cleanString($rowCus[CustomerFirstNameChange])) . "', CustomerLastNameChange = '" . addslashes(cleanString($rowCus[CustomerLastNameChange])) . "', CustomerAddressChange = '" . addslashes(cleanString($rowCus[CustomerAddressChange])) . "', CustomerCityChange = '" . addslashes(cleanString($rowCus[CustomerCityChange])) . "', CustomerStateChange = '" . $rowCus[CustomerStateChange] . "', CustomerZIPChange = '" . $rowCus[CustomerZIPChange] . "', CustomerPhone = '" . $rowCus[CustomerPhone] . "', CustomerEmail = '" . addslashes(cleanString($rowCus[CustomerEmail])) . "', Scanned = 'T', CustomerYearMakeModel = '" . $rowCus[CustomerYearMakeModel] . "', ScanDate = '" . $rowCus[ScanDate] . "', StartTime = '" . $rowCus[StartTime] . "', SalespersonID = '" . $rowCus[SalespersonID] . "' where jobs_no = '" . $rowCus[jobs_no] . "' AND CustomerKey = '" . $rowCus[CustomerKey] . "'";
		$sql[] = "|insert into tcustomer (CustomerKey, jobs_no, subjobs_no, CustomerFirstNameChange, CustomerLastNameChange, CustomerAddressChange, CustomerCityChange, CustomerStateChange, CustomerZIPChange, ScanDate, SalespersonID, Scanned, StartTime, CustomerYearMakeModel, CustomerEmail, CustomerPhone) VALUES ('" . $rowCus[CustomerKey] . "','" . $rowCus[jobs_no] . "','" . $rowCus[subjobs_no] . "','" . addslashes(cleanString($rowCus[CustomerFirstNameChange])) . "','" . addslashes(cleanString($rowCus[CustomerLastNameChange])) . "','" . addslashes(cleanString($rowCus[CustomerAddressChange])) . "','" . addslashes(cleanString($rowCus[CustomerCityChange])) . "','" . $rowCus[CustomerStateChange] . "','" . $rowCus[CustomerZIPChange] . "','" . $rowCus[ScanDate] . "','" . $rowCus[SalespersonID] . "','" . $rowCus[Scanned] . "','" . $rowCus[StartTime] . "','" . $rowCus[CustomerYearMakeModel] . "','" . addslashes(cleanString($rowCus[CustomerEmail])) . "','" . $rowCus[CustomerPhone] . "')";
		$updateLoc = "update tcustomer set Uploaded = 'T' where jobs_no = '" . $rowCus[jobs_no] . "' AND CustomerKey = '" . $rowCus[CustomerKey] . "'";
		$resultLoc = $mysql->query($updateLoc);
	}
}
// END CLASSIC TEMPLATE SYNC


// SURVEY SYNC

unset($insertList, $updateList);
$selectSurvey = "select * from tcustomersurvey where Uploaded = 'F'";
$resultSurvey = $mysql->query($selectSurvey);
if ($resultSurvey->num_rows > 0) {
	while ($rowSurvey = $resultSurvey->fetch_array()) {
		$insertList[] = "('" . $rowSurvey[jobs_no] . "','" . $rowSurvey[subjobs_no] . "','" . $rowSurvey[CustomerKey] . "','" . $rowSurvey[mvquestion_no] . "','" . $rowSurvey[mvanswer_no] . "')";
		$updateList[] = $rowSurvey[tcustomersurvey_no];
	}
	$sql[] = "insert into tcustomersurvey (jobs_no, subjobs_no, CustomerKey, mvquestion_no, mvanswer_no) VALUES " . implode(",",$insertList);
	$updateLoc = "update tcustomersurvey set Uploaded = 'T' where tcustomersurvey_no in (" . implode(",",$updateList) . ")";
	$resultLoc = $mysql->query($updateLoc);
}
// END SURVEY SYNC


// START PRIZE DISTRO SYNC

$selectDistro = "select * from tprizedistro where tprizedistro_uploaded = 'F'";
$resultDistro = $mysql->query($selectDistro);
if ($resultDistro->num_rows > 0) {
	while ($rowDistro = $resultDistro->fetch_array()) {
		$sql[] = "update tprizedistro set CustomerKey = '" . $rowDistro[CustomerKey] . "', tprizedistro_awarded = '" . $rowDistro[tprizedistro_awarded] . "', tprizedistro_ts = '" . $rowDistro[tprizedistro_ts] . "', tprizedistro_uploaded = 'T' where tprizedistro_no = '" . $rowDistro[tprizedistro_no] . "'";
		$updateLocal = "update tprizedistro set tprizedistro_uploaded = 'T' where tprizedistro_no = '" . $rowDistro[tprizedistro_no] . "'";
		$resultLocal = $mysql->query($updateLocal);
	}
}

// END PRIZE DISTRO SYNC

/* TESTING
$select = "select * from test where Uploaded='F'";
$result = $mysql->query($select);
if ($result->num_rows > 0) {
	while($row = $result->fetch_array()) {
		$sql[] = "insert into test2 (test_no,data) VALUES ('" . $row[test_no] . "','" . addslashes(cleanString($row[data])) . "')";
echo " i have results";
	}
}
*/

$sqlStr = implode(';',$sql);

$url = "https://getfastcredit.com/data-receiver.php";

$post = array('key'=>$key,'sql'=>$sqlStr);
$pageHandle = curl_init();
curl_setopt($pageHandle, CURLOPT_URL, $url);
curl_setopt($pageHandle, CURLOPT_POST, true);
curl_setopt($pageHandle, CURLOPT_HTTPHEADER, array("Content-Type:  multipart/form-data"));
curl_setopt($pageHandle, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($pageHandle, CURLOPT_POSTFIELDS, $post);
curl_setopt($pageHandle, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
curl_setopt($pageHandle, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($pageHandle, CURLOPT_SSL_VERIFYHOST, false);

$page = curl_exec($pageHandle);
curl_close($pageHandle);

?>