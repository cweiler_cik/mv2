<?php

// http://hades.triauto.net/mv/lib/updateCustomer.php?job=81080&drop=1&sp=75139&customer-key=81080000040&customer-fname=Test&customer-lname=Testerson&customer-address=555 Somewhere St&customer-city=Indianapolis&customer-state=IN&customer-zip=46227&year=2004&make=SATURN&model=ION&mileage=70000&customer-email=testcustomer@tester.com&phone=3177024504&cell=3177024504&startTime=1368536747219&endTime=1368536808331
require_once( 'functions.php' );

$dbh 	= new cikPdo;
$dbh 	= $dbh->dbh;
$params	= $_REQUEST;

$job				= $params['job'];
$drops_no			= $params['drop'];
$scanned 			= 'T';
$key				= $params['customer-key'];
$fname 				= $params['customer-fname'];
$lname 				= $params['customer-lname'];
$address 			= $params['cutsomer-address'];
$city 				= $params['customer-city'];
$state 				= $params['customer-state'];
$zip 				= $params['customer-zip'];
$email 				= $params['customer-email'];
$year 				= $params['year'];
$make 				= $params['make'];
$model 				= $params['model'];
$mileage			= $params['mileage'];
$yearMakeModel 		= $year . ' ' . $make . ' ' . $model;
$phone 				= $params['phone'];
$cell 				= $params['cell'];
// $scanDate 		= $params['scanDate'];
$scanDate 			= date( 'm-d-Y', time() );
$start 				= $params['startTime'];
$finish 			= mktime( date("H"), date("i"), date("s"), date("m"), date("d"), date("Y") );
$spId 				= $params['sp'];
$test 				= 'F';
$followUp 			= 'F';

$params['scanDate'] = $scanDate;

if ($params['subjob']):
	$subjob = $params['subjob'];
else:
	$subjob = 1;
endif;

$q = "UPDATE `tcustomer` SET
		`Scanned` 					= :scanned,
		`CustomerFirstNameChange` 	= :fname,
		`CustomerLastNameChange` 	= :lname,
		`CustomerAddressChange`		= :address,
		`CustomerCityChange` 		= :city,
		`CustomerStateChange`		= :state,
		`CustomerZIPChange` 		= :zip,
		`CustomerYearMakeModel` 	= :yearMakeModel,
		`CustomerEmail` 			= :email,
		`CustomerPhone` 			= :phone,
		`CustomerCell` 				= :cell,
		`ScanDate` 					= :scanDate,
		`StartTime` 				= :start,
		`EndTime` 					= :finish,
		`SalespersonID` 			= :spId,
		`TestRecord` 				= :test,
		`FollowUp` 					= :followUp
	WHERE `CustomerKey` = :key
	AND `jobs_no` 		= :job";

try {
	$stmt = $dbh->prepare( $q );

	$stmt->bindParam( ':key', $key );
	$stmt->bindParam( ':job', $job );
	$stmt->bindParam( ':scanned', $scanned );
	$stmt->bindParam( ':fname', $fname );
	$stmt->bindParam( ':lname', $lname );
	$stmt->bindParam( ':address', $address );
	$stmt->bindParam( ':city', $city );
	$stmt->bindParam( ':state', $state );
	$stmt->bindParam( ':zip', $zip );
	$stmt->bindParam( ':yearMakeModel', $yearMakeModel );
	$stmt->bindParam( ':email', $email );
	$stmt->bindParam( ':phone', $phone );
	$stmt->bindParam( ':cell', $cell );
	$stmt->bindParam( ':scanDate', $scanDate );
	$stmt->bindParam( ':start', $start );
	$stmt->bindParam( ':finish', $finish );
	$stmt->bindParam( ':spId', $spId );
	$stmt->bindParam( ':test', $test );
	$stmt->bindParam( ':followUp', $followUp );	
	
	$result = $stmt->execute();
} catch ( PDOException $e ) {
	// return json_encode($e->getMessage());
	exit(json_encode($e->getMessage()));
}

if ( $params['user_agent'] != 'ie' ):
	header( 'Content-type: application/json' );
else:
	header( 'Content-type: text/plain' );
endif;

if ( $result && $stmt->rowCount() > 0 ) {
	// return true;
	// return json_encode(array( 'success' => true, 'message' => $stmt->rowCount() . ' rows affected' ));
	exit( json_encode(array( 'success' => true, 'message' => $stmt->rowCount() . ' rows affected', 'result' => $result )) );
} else {
	// return false;
	exit( json_encode(array( 'success' => false, 'message' => 'No rows affected' )) );
}

// var_dump( $json );
// exit;


exit( $json );