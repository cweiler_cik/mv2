<?php

// http://hades.triauto.net/mv/lib/testSave.php?job=81080&drop=1&sp=75139&customer-key=81080000025&customer-fname=Test&customer-lname=Testerson&customer-address=555 Somewhere St&customer-city=Indianapolis&customer-state=IN&customer-zip=46227&year=2004&make=SATURN&model=ION&mileage=70000&customer-email=testcustomer@tester.com&phone=3177024504&cell=3177024504&startTime=1368536747219&endTime=1368536808331
require_once( 'functions.php' );

$params = $_REQUEST;
$json = json_encode( $params );

if ( $params['user_agent'] != 'ie' ):
	header( 'Content-type: application/json' );
else:
	header( 'Content-type: text/plain' );
endif;

exit( $json );

?>

<div id="#log"></div>

<script>
$(function(){
	var $log = $('#log');
	var result = {};

	$.ajax({
		url: 'lib/testSave.php',
		data: <?php echo json_encode( $params ) ?>,
		success: function(e, data){
			console.log('data', data);

			$.each(data, function(k,v){
				var ul = $('<ul id="log-list" />');
				var li = $('<li/>');

				li.attr('id', k + '-log').val(v).html(v);
				ul.append(li);

				result[k] = v;

				$('#log').append(div);
			});
		}
	});
});
</script>