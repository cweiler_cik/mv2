<?php

// http://hades.triauto.net/kiosk/lib/createSp.php?sp-fname=Testo&sp-lname=Molesto&sp-email=testo@molesto.com
//require_once( 'functions.php' );

$params = $_REQUEST;

if ( $params['user_agent'] != 'ie' ):
	header( 'Content-type: application/json' );
else:
	header( 'Content-type: text/plain' );
endif;

$dbh = new cikPdo;
$dbh = $dbh->dbh;
$fname = $params['sp-fname'];
$lname = $params['sp-lname'];
$email = $params['sp-email'];

// $mysql = new mysqli('localhost', 'root', 'fl1p0Ut', 'hades');
// $q = $mysql->prepare( "INSERT INTO `salespersons` (`FirstName`, `LastName`, `Email`) VALUES (?, ?, ?)" );
// $q->bind_param('s', $fname);
// $q->bind_param('s', $lname);
// $q->bind_param('s', $email);
// $q->execute();
// $q->bind_result( $result );
// $q->fetch();

// exit ( json_encode( $result ) );

$q = "INSERT INTO `salespersons` (
		FirstName,
		LastName,
		Email
	) VALUES (
		:fname,
		:lname,
		:email
	)";

try {

	$stmt = $dbh->prepare( $q );
	$stmt->bindParam( ':fname', $fname );
	$stmt->bindParam( ':lname', $lname );
	$stmt->bindParam( ':email', $email );
	$stmt->execute();

} catch ( PDOException $e ) {
	exit( $e->getMessage() );
}

$select = "SELECT SalepersonID FROM `salespersons` WHERE FirstName = :fname AND LastName = :lname AND Email = :email";

try {

	$sel = $dbh->prepare( $select );
	$sel->bindParam( ':fname', $fname );
	$sel->bindParam( ':lname', $lname );
	$sel->bindParam( ':email', $email );
	$sel->execute();

} catch ( PDOException $e ) {
	exit( $e->getMessage() );
}

$result = $sel->fetchAll( PDO::FETCH_ASSOC );

if ( $result ):
	foreach ( $result as $row ) {
		$res = $row;
	}
else:
	exit( false );
endif;

exit(json_encode( $res ));