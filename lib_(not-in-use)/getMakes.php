<?php

require_once('functions.php');

$params = $_REQUEST;
$year = $params['year'];

$makes = getMakes( $year );
$json = json_encode( $makes );

if ( $params['user_agent'] != 'ie' ):
	header( 'Content-type: application/json' );
else:
	header( 'Content-type: text/plain' );
endif;

exit( $json );