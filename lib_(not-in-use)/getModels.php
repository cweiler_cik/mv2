<?php

require_once('functions.php');

$params = $_REQUEST;
$year = $params['year'];
$make = $params['make'];

$models = getModels( $year, $make );
$json = json_encode( $models );

if ( $params['user_agent'] != 'ie' ):
	header( 'Content-type: application/json' );
else:
	header( 'Content-type: text/plain' );
endif;

exit( $json );