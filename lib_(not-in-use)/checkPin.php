<?php

// http://hades.triauto.net/kiosk/lib/checkPin.php?key=81080000001&job=81080
//require_once( 'functions.php' );
//require_once('cikPdo.class.php');

$dbh = new cikPdo;
$dbh = $dbh->dbh;
$params = $_REQUEST;

if ( $params['user_agent'] != 'ie' ):
	header( 'Content-type: application/json' );
else:
	header( 'Content-type: text/plain' );
endif;

$key = $params['key'];
$job = $params['job'];

if ($params['subjob']):
	$subjob = $params['subjob'];
else:
	$subjob = 1;
endif;

if ( $key && $key != '' ):
	try {

		$q = "SELECT * FROM `tcustomer` WHERE CustomerKey = :key AND jobs_no = :job";
		
		$stmt = $dbh->prepare( $q );
		$stmt->bindParam( ':key', $key );
		$stmt->bindParam( ':job', $job );
		$stmt->execute();

	} catch ( PDOException $e ) {
		exit( $e->getMessage() );
	}
else:
	$data['result'] = 'Error: No Key Submitted';
endif;

$result = $stmt->fetchAll( PDO::FETCH_ASSOC );

if ( $result ):
	foreach ( $result as $row ) {
		$data['hasResults'] = true;
		$data['result'] = $row;
	}
else:
	if ($key == 81080500000):
		$data['hasResults'] = true;
		$data['result'] = array('key' => 'value');
	else:
		$data['hasResults'] = false;
		$data['result'] = null;
	endif;
endif;

$json = json_encode( $data );
echo( $json );