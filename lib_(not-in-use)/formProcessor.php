<?php

// http://hades.triauto.net/mv/lib/formProcessor.php?pin=81080000041&salesperson=&sp-fname=&sp-lname=&sp-email=&edit-customer-fname=&edit-customer-lname=&edit-customer-address=&city=&state=&zip=&phone=(654)+321-3216&email=&year=2011&make=AUDI&model=A8-V8&mileage=100000&when=3months&condition=used&mechanic-select=Express+lube%2Fdiscount+store&prize-id=&prize-name=&contact-email=m%40mail.com
require_once('functions.php');

$params = $_REQUEST;

if ( $params['needs-created'] == 'true' ) {
	$operation = createCustomer( $params );
} else {
	$operation = updateCustomer( $params );
}

if ( $params['isWinner'] == 'true' ) {
	$updates = updatePrizes( $params );
} else {
	$updates = updatePrizeLog( $params );
}

$result = array(
	'operation' => $operation,
	'updates'	=> $updates,
	'debug'		=> $params
	);

exit( json_encode( $result ) );