<?php

//require_once( 'functions.php' );
//require_once('cikPdo.class.php');

$dbh = new cikPdo;
$dbh = $dbh->dbh;
$params = $_REQUEST;

if ( $params['user_agent'] != 'ie' ):
	header( 'Content-type: application/json' );
else:
	header( 'Content-type: text/plain' );
endif;

$fname = $params['sp-fname'];
$lname = $params['sp-lname'];
$email = $params['sp-email'];

if ( $email && $email != '' ):
	$q = "SELECT * FROM `salespersons` WHERE FirstName = :fname AND LastName = :lname AND Email = :email";
else:
	$q = "SELECT * FROM `salespersons` WHERE FirstName = :fname AND LastName = :lname";
endif;

echo $q."<br>";

try {

	$stmt = $dbh->prepare( $q );
	$stmt->bindParam( ':fname', $fname );
	$stmt->bindParam( ':lname', $lname );

	if ( $email && $email != '' ):
		$stmt->bindParam( ':email', $email );
	endif;

	$stmt->execute();

} catch ( PDOException $e ) {
	$result = $e->getMessage();

	return $result;
}

$result = $stmt->fetchAll( PDO::FETCH_ASSOC );

if ( $result ):
	foreach ( $result as $row ) {
		$res[] = $row;
	}
endif;

exit( json_encode( $res ) );