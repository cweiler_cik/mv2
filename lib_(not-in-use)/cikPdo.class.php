<?php

/*
class cikPdo extends PDO
{
	public $dbh;

	public function __construct()
	{
		$db = 'hades';
		$dsn = "mysql:dbname={$db};host=127.0.0.1";
		$user = 'root';
		$password = 'fl1p0Ut';

		try {
			$dbh = new PDO( $dsn, $user, $password );
		} catch ( PDOException $e ) {
			echo 'Connection Failed: ' . $e->getMessage();
		}

		$this->dbh = $dbh;

		return $this;
	}
}
*/

class cikPdo extends PDO
{
	public $dbh;

	public function __construct()
	{
		$db = $kDBName;
		$dsn = "mysql:dbname={$db};host=$kDBServer";
		$user = $kDBUser;
		$password = $kDBPass;

		try {
			$dbh = new PDO( $dsn, $user, $password );
		} catch ( PDOException $e ) {
			echo 'Connection Failed: ' . $e->getMessage();
		}

		$this->dbh = $dbh;
		
		return $this;
	}
}

?>