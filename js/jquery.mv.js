(function($){
	$.fn.mvForm = function(options)	{
		if ($.isFunction(options)) {
			callback = options;
			options = null;
		} else {
			options = $.extend($.fn.mvForm.defaults, options);
			callback = options.complete;
		}

		options = $.extend($.fn.mvForm.defaults, options);

		return this.each(function() {
			var $this = $(this);
			var current = options.current;
			var next = Number(current + 1);
			var prev = Number(current - 1);
			
			// var steps = $('fieldset', this).length;
			var steps = $('.content', this).length;

			$(options.nextBtn).off('click');
			$(options.backBtn).off('click');
			$(options.rstBtn).off('click');
			
			console.log('Current Step', current);
			console.log('Next Step', next);
			console.log('Prev Step', prev);
			console.log('Total Steps', steps);

			$($this).data('step', current);
			$('#step' + current).css('display', 'table');			

			// if (current <= 1) {
			// 	options.backBtn.fadeOut(100);
			// } else {
			// 	options.backBtn.fadeIn(100);
			// }

			// if (Number(current + 1) == steps) {
			// 	options.nextBtn.fadeOut(100);
			// } else {
			// 	options.nextBtn.fadeIn(100);
			// }

			$(options.nextBtn).one({
				click: function(e){
					e.preventDefault();
					var current 	= $($this).data('step');
					var $current 	= $('#step' + current);
					var valid, timer;

					timer = $('#promotion').data('timer');
					clearTimeout(timer);

					console.log('timer', timer);
					
					$('input', $current).each(function(){
						valid = $(this).valid();
						console.log($(this).attr('id'), valid);

						$(this).removeClass('unvalidated');

						if ($(this).hasClass('invalid')){
							valid = false;
						} else if ($(this).hasClass('valid')){
							valid = true;
						}
						
						// if (valid){
						// 	$(this).removeClass('invalid').addClass('valid');
						// } else {
						// 	$(this).removeClass('valid').addClass('invalid');
						// }
					});

					$('select', $current).each(function(){
						var $select;

						console.log('Select.valid()', $(this).valid());
						$select = $('#' + $(this).attr('id') + 'SelectBoxIt');
						$sel 	= $(this).data('selectBox-selectBoxIt');
						valid 	= $(this).valid();

						$(this).on({
							change: function(e){
								valid = $(this).valid();
								$(this).removeClass('unvalidated');
								$select.removeClass('unvalidated');

								// console.log('validation dump', {
								// 	$select: $select,
								// 	$sel: $sel
								// });

			                    $(this).removeClass('unvalidated');
			                    $select.removeClass('unvalidated');
			                    
			                    if (valid){
			                        $(this).removeClass('invalid').addClass('valid');
			                        $select.removeClass('invalid').addClass('valid');
			                        $sel.refresh();
			                    } else {
			                        $(this).removeClass('valid').addClass('invalid');
			                        $select.removeClass('valid').addClass('invalid');
			                        $sel.refresh();
			                    }

								// if (valid){
								// 	$select.removeClass('invalid').addClass('valid');
								// 	$sel.refresh();
								// } else {
								// 	$select.removeClass('valid').addClass('invalid');
								// 	$sel.refresh();
								// }
							}
						});
					});
					
					console.log('has valid class', $(this).hasClass('valid'));

					if (valid) {
						$('input', $current).each(function(){

						});

						$('#step' + current).fadeOut().hide();
												
						setTimeout(function(){
							var current = $($this).data('step');
							var $current = $('#step' + current);
							var next = Number(current + 1);
							
							if (next <= 1) {
								options.backBtn.fadeOut(100);
							} else {
								options.backBtn.fadeIn(100);
							}

							if (next == steps) {
								options.nextBtn.fadeOut(100);
							} else {
								options.nextBtn.fadeIn(100);
							}

							console.log('The next step', next);
							$('#step' + next).fadeIn().css('display', 'table');

							current = ++current;
							next = ++next;
							
							console.log('After change', {
								'current': current,
								'next': next,
								'steps': steps
							});

							if (current > 1) {
								if ($(options.rstBtn).css('display') == 'none'){
									$(options.rstBtn).fadeIn(300);
								}
							}

							console.log('Extra Current Step', current);

							$.extend($($this).data(), {step: current});
						}, 400);
					}

					if ($('#promotion').data('step') == 2) {
			            var landing;

						$.getJSON('../_tools/globals/_fns/prize_function.php', {
						    key: $('#promotion').data('customer-key')
						}, function (data) {
							console.log('data returned from prize_function', data);
							// MAKE IT AN ARRAY WITH THE prizeid AND THE name = $('#promotion').data('customer-prize')
							landing = (parseInt(prize_cnt) * 8) + parseInt(data.prize_id);
							//console.log('landing: ' + landing);
							$('#promotion').data('customer-prizeid', landing);
							$('#promotion').data('customer-prize', data.prize_name);
							$('#promotion').data({
								'prizeClaimNumber' 			: data.prizeClaimNumber,
								'prizeClaimNumberEntered'	: data.prizeClaimNumberEntered,
								'prizeDesc' 				: data.prizeDesc,
								'isWinner' 					: data.isWinner,
								'whichPlay'					: data.whichPlay,
								'odds' 						: data.odds,
								'playOdds' 					: data.playOdds,
								'playerNumber' 				: data.playerNumber,
								'winnerNumber' 				: data.winnerNumber
							});
						});
		            
		            } else 	if ($('#promotion').data('step') == 7) {
		                $('span', '#review-table tbody td').each(function(k, v){
		                    var id = $(v).attr('id');
		                    var data = $($this).data(id);

		                    console.log('DATA', data);
		                    
		                    $(this).html(data);
		                });

		            } else if ($('#promotion').data('step') == 9) { // current step == 9	                
		                		                
		                $('.selected', '#buzz-table').each(function(){
		                    var input = $('<input type="hidden"/>').attr('name', $(this).attr('name')).val(true);

		                    $('#buzz-table').append(input);
		                });

		                var object = {};
		                $('input[type=hidden]', '#buzz-table').each(function(){
		                	var name = $(this).attr('name');
		                	var val = $(this).val();

		                	object[name] = val;
		                });

		                $($this).data('buzz', object);
		            } else if ($('#promotion').data('step') == 10) { // current step == 10
		                // $('span', '#step 10 #next-btn').html('finish').on('click', function(e){
		                //     e.preventDefault();
		                //     var style = params.style;
		                //     var obj = {};
		                //     $.extend(obj, {'style': style, step: 'init'});
		                //     console.log('Restart Obj', obj);

		                //     $('#reload').trigger('navNext', {step: 1, style: style});
		                // });
		            }

					$(document).trigger('formChange', $($this).data());
				}
			});

			$(options.backBtn).one({
				click: function(e){
					e.preventDefault();
					var current = $($this).data('step');
					var prev = Number(current - 1);

					// console.log('OnClick Current', ('#step' + current));
					// console.log('PREV', prev);

					$('#step' + current).fadeOut().hide();
					$('input','#step' + current)
						.not(':button, :submit, :reset')
						.each(function(){
							$(this).val('');
						});

					if (prev <= 1) {
						options.backBtn.fadeOut(100);
					} else {
						options.backBtn.fadeIn(100);
					}

					setTimeout(function(){
						var current = $($this).data('step');
						var prev = Number(current - 1);

						if (current > 1) {
							$('#step' + prev).fadeIn().css('display', 'table');
						}

						console.log('prev step', prev);
						
						$.extend($($this).data(), {'step': prev});

						// if (current == steps) {
						// 	options.nextBtn.fadeOut(100);
						// } else {
						// 	options.nextBtn.fadeIn(100);
						// }
					}, 400);

					$(document).trigger('formChange', $($this).data());
				}
			});

			$(options.rstBtn).one({
				click: function(e){
					e.preventDefault();
					location.reload();
					// var current = $($this).data('step');

					// $('#step' + current).hide('fadeOut');

					// setTimeout(function(){
					// 	$('#step1').css('display', 'table');

					// 	current = 1;
					// 	$($this).removeData();
					// 	$($this).data('step', current);
					// }, 400);

					// $(document).trigger('formChange', $($this).data());
				}
			});

			$(options.saveBtn).off('click').one({
				click: function(e){
					// $(this).off('click');
					// e.preventDefault();
                    console.log('Save Started');
					e.preventDefault();

                    var finish = $.now();
                    var allData = {};

                    $('#promotion').data('endTime', finish);
                    console.log('endTime', finish);

                  //   $.each($('#promotion').data(), function(key,value){
                  //   	// var input;

                  //   	// if (k != 'sp' && v != '' && v != null && v != undefined){
                  //   	// 	input = $('<input type="hidden"/>');
                  //   	// 	input.attr('name', k + '-value').val(v);
                    		
                  //   	// 	console.log('new input key', k);
                  //   	// 	console.log('new input val', v);
                  //   	// 	// console.log('is Empty Object', $.isEmptyObject(v));
                  //   	// 	console.log('Type', $.type(v));

                  //   	// 	$('#promotion').append(input);

                  //   	// 	// if ($.isEmptyObject(v) === false && $.isPlainObject(v) === false){
                  //   	// 	// if ($.type(v) === 'string' || $.type(v) === 'number' || $.type(v) === 'boolean'){
                  //   	// }

                		// if ($.type(value) != 'object'){
                		// 	allData[key] = value;
                		// }
                  //   });

                    allData = $('#promotion').data();
                    console.log('allData', allData);

                    var $post = {};

                    $.each(allData, function(key, value){
                        if ($.type(value) != 'object' && $.type(value) != 'array'){
                            $post[key] = value;
                        }
                    });

                    console.log('Form Submitted');
                    console.log('Form Submitted Data', $post);

                    $.ajax({
                        //url: 'lib/formProcessor.php',
                        url: '_inc/_ajx/ajxFormActions.php?action=process_post', 
                        data: $post,
                        error: function(request, type, errorThrown){
                            console.log('Error Data', {
                                request: request,
                                type: type,
                                error: errorThrown
                            });
                        }
                    }).done(function(data){
                        console.log('Submit Return Data', data);
                        location.reload();
                    });
                    

                    // $('#promotion').submit(allData);
                    // $('#promotion').submit();
                    //console.log('Script is Finished');

                    // $.ajax({
                    //     url: 'lib/formProcessor.php',
                    //     data: allData,
                    //     type: 'json',
                    //     async: false,
                    //     error: function(request, type, errorThrown){
                    //         console.log('Error Data', {
                    //             request: request,
                    //             type: type,
                    //             error: errorThrown
                    //         });
                    //     }
                    // }).done(function(data){
                    //     console.log('Submit Return Data', data);
                    // });                    

					// $('#promotion').submit();

				}
			});

			if ($.isFunction(callback)) {
				callback.call(this);
			}
		});
	}

	$.fn.mvForm.defaults = {
		current : 1,
		nextBtn	: $('#NxtBtn'),
		backBtn	: $('#BckBtn'),
		rstBtn	: $('#RstBtn'),
		saveBtn	: $('#save-btn'),
		//action	: 'lib/formProcessor.php',
		action	: '_inc/_ajx/ajxFormActions.php?action=process_post',
		complete: null,
	};
})(jQuery);