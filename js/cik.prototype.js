$(function() {
	$('#reload').on({
		navNext: function(e, options){
			var data = options;
			var step = options.step;
			var url = 'content/step' + step + '.php';

			console.log('Next URL', url);
			console.log('NavNext Options', data);
			console.log('NavNext Step', step);
			console.log('Hash', window.hash);
			
			// $(this).load(url, data);
			$.ajax({
				url: url,
				data: data,
				type: 'POST',
				context: $('#reload'),
				dataType: 'text'
				success: function(response, status)
				{
					console.log({
						response: response,
						status: status
					});

					$(this).html(response);
					$(this).kiosk('initNav', params);
				}
			});
		},
		navBack: function(e, options){
			var data = options;
			var step = options.step;
			console.log('Back Step', step);
			if (step == 0 || step == '0' || step == null || step == undefined) {
				step = 'init';
				var url = '#';
			} else {
				var url = 'content/step' + step + '.php';
			}
			console.log('Back URL', url);
			console.log('NavBack Options', data);
			console.log('NavBack Step', step);
			console.log('Hash', window.hash);

			// $(this).load(url, data);
			$.ajax({
				url: url,
				data: data,
				type: 'POST',
				context: $('#reload'),
				dataType: 'text'
			}).done(function(response, status){
				console.log({
					response: response,
					status: status
				});

				$(this).html(response);
				$(this).kiosk('initNav', params);
			});
		},
		restart: function(e){
			e.preventDefault();
			var trigger = $(e.target);
		}
		// loadNext: function(e, data){
		//	var url = data.url;
		//	var params = data.params;
		//	var trigger = $(e.target);

		// 	$(trigger).nav('next', {'url': url, 'params': params});
		// },
		// loadPrev: function(e, data){
		// 	console.log('loadPrev Data', data);
		// 	var url = data.url;
		// 	var params = data.params;
		// 	var trigger = $(e.target);

		// 	console.log('Prev URL', url);

		//	$(trigger).nav('back', {'url': url, 'params': params});
		// }
	});
});