<script type="text/javascript">
	$(function(){
	    var $params = <?php echo json_encode($params) ?>;
	    var $timer;
	
	    $.extend($('#promotion').data(), $params);
	
	    $('.validator, .kiosk-select').each(function(){
	        // console.log('Iterated Here', $(this).attr('id'));
	        $(this).attr('autocomplete', 'off');
	    });
	
	    $('#BckBtn').hide();
	    $('#RstBtn').fadeOut(50);
	    $('#promotion').mvForm();
	    $('#promotion').on({
	        keypress: function(e) {
	            if (e.keyCode == 13) {
	                e.preventDefault();
	                $('#NxtBtn').click();
	            }
	        }
	    });
	
	    $('input', '#step1').first().focus();
	
	    // ================ FORM VALIDATION ================ //
	    $.validator.addMethod('valueNotEquals', function(value, element, arg){
	        return value != '' && value != null && value != undefined;
	    }, 'Value must not equal arg.');
	
	    $.validator.addMethod('validResult', function(value, element, arg){
	        // console.log('validResult value', value);
	        // console.log('validResult element', element);
	        // console.log('validResult arg', arg);
	        
	        if (arg == true){
	            $(element).trigger('change');
	
	            console.log('hasData', $(element).data('hasData'));
	
	            if ($(element).data('hasData') ){
	                // console.log('validResult', 'true');
	                return true;
	            } else {
	                // console.log('validResult', 'false');
	                return false;
	            }
	        } else {
	            return true;
	        }
	    });
	
	    var $validator = $('#promotion').validate({
	        debug: true,
	        rules: {
	            pin: {
	                required: true,
	                minlength: 1,
	                maxlength: 11,
	                number: true
	            },
	            city: {
	                required: true,
	            },
	            state: {
	                required: true,
	            },
	            zip: {
	                required: true,
	                number: true
	            },
	            phone: {
	                /*
					required: function(){
	                    return $('#email').val().length <= 0
	                }
					*/
					required: false,
	                phoneUS: true
	            },
	            email: {
	                /*
					required: function(){
	                    return $('#phone').val().length <= 0
	                }
					*/
					required: true,
	                email: true
	            },
	            year: {
	                required: true,
	                valueNotEquals: '',
	            },
	            make: {
	                required: function(){
	                    return $('#year').val().length > 0;
	                },
	                valueNotEquals: ''
	            },
	            model: {
	                required: function(){
	                    return $('#make').val().length > 0;
	                },
	                valueNotEquals: ''
	            },
	            mileage: {
	                required: function(){
	                    return $('#model').val().length > 0;
	                },
	                valueNotEquals: ''
	            },
	            when: {
	                required: true,
	                valueNotEquals: ''
	            },
	            condition: {
	                required: true,
	            },
	            'mechanic-select': {
	                required: true,
	                valueNotEquals: ''
	            },
	            'contact-email': {
	                required: true,
	                email: true
	            },
	        },
	        errorClass: 'invalid',
	        validClass: 'valid',
	        onsubmit: false,
	        // onkeyup: true,
	        // onclick: true,
	        // onfocusout: true,
	        errorElement: 'span',
	        wrapper: 'em',
	        errorPlacement: function(error, element)
	        {
	            console.log('error', error.html());
	            console.log('element', element);
	
	            var div = $('<div class="error-msg" />');
	            div.attr('for', $(element).attr('name'));
	
	            console.log('invalidCount', $validator.numberOfInvalids());
	
	            if (div.html() != '<em>' + error.html() + '</em>' || div.html() != '<em style="display: inline;">' + error.html() + '</em>'){
	                error.appendTo(div);
	                div.appendTo($(element).parent());
	
					if ($('#promotion').data('step') == 4) {
						//console.log($(element).attr('name'));
						if($(element).attr('name') == 'phone' && $(element).val() != ''){
							$('div[for="phone"]').css('float', 'right');
							$('div[for="phone"]').css('margin-bottom', '10px');
							$('div[for="phone"]').css('width', '87%');
						}
						if($(element).attr('name') == 'email'){
							$('div[for="email"]').css('width', '87%');
							$('div[for="email"]').css('float', 'right');
						}
					}
	
	            } else {
	                console.log('Suppressed Error: ', error);
	            }
	        },
	        // success: 'valid',
	        highlight: function(element, errorClass, validClass)
	        {
	            console.log('highlight fired!');
	            
	            $(element).removeClass('unvalidated').removeClass(validClass).addClass(errorClass);
	            // $(element.form).find("label[for=" + element.id + "]").addClass(errorClass);
	        },
	        unhighlight: function(element, errorClass, validClass)
	        {
	            console.log('unhighlight fired!');
	        //     console.log('error span', $('span[for=' + $(element).attr('name') + '].invalid'));
	            
	            $(element).removeClass(errorClass).addClass(validClass);
	        //     $('span[for=' + $(element).attr('name') + '].invalid').remove();
	
	        // //     console.log('error span', $('span.invalid[for=' + $(element).attr('name') + ']', $(element).parent()).html(''));
	        // //     $('span.invalid[for=' + $(element).attr('name') + ']', $(element).parent()).html('');
	        // //     // $(element.form).find("label[for=" + element.id + "]").removeClass(errorClass);
	        },
	        messages: {
	            pin: 'Please input a valid PIN or click the link below',
	            salesperson: 'Please select a salesperson from the list',
	            'sp-fname': 'Please enter the salesperson\'s first name',
	            'sp-lname': 'Please enter the salesperson\'s last name',
	            'sp-email': 'Please enter the salesperson\'s email',
	            'edit-customer-fname': 'Please enter your first name',
	            'edit-customer-lname': 'Please enter your last name',
	            'edit-customer-address': 'Please enter your address',
	            city: 'Please enter your city',
	            state: 'Please enter your state',
	            zip: 'Please enter your zip code',
	            phone: 'Please enter your phone number',
	            email: 'Please enter your email',
	            year: 'Please enter your vehicle\'s year',
	            make: 'Please enter your vehicle\'s year',
	            model: 'Please enter your vehicle\'s year',
	            mileage: 'Please enter your vehicle\'s year',
	            when: 'Please enter when you would consider purchasing',
	            condition: 'Please enter your vehicle\'s condition',
	            'mechanic-select': 'Please select who services your vehicle',
	            'contact-email': 'Please tell us where to contact you'
	        }
	    });
	    // ================ END FORM VALIDATION ================ //
	    
	    $('#pin').one({
	        keydown: function(e){
	            var time = $.now();
	            var date = new Date;
	            var month = date.getMonth();
	            var day = date.getDate();
	            var year = date.getFullYear();
	
	            month = Number(month + 1);
	
	            console.log('date', date);
	            console.log('month', month);
	            console.log('day', day);
	            console.log('year', year);
	
	            finalDate = month + '/' + day + '/' + year;
	
	            $('#promotion').data('startTime', time).data('scanDate', finalDate);
	
	            console.log('scanDate', finalDate);
	            console.log('startTime', time);
	        }
	    });
	
	    // Check/filter PIN and call customer data from DB
	    $('#pin').on({
	        keypress: function(e){
	            if (e.which == 13){
	                $(this).blur();
	            }
	        },
	        blur: function(e, data){
	            console.log('Change triggered!');
	
	            var $this = $(this);
	            var customerKey;
	            var val = $(this).val();
	            var dealerId = 81080;
	            var job = val.substring(0, 5);
	            var substr = val.substring(5);
	
	            console.log('val', val);
	            console.log('substring', val.substring(5));
	            console.log('val.length', val.length);
	
	            if (val.length == 11){
	                // Save to DB
	                customerKey = val;
	                dealerId = job;
	            } else if (val.length < 11 && val.length >= 6){
	                // var num = val.substring(0, val.lastIndexOf('0'));
	                var num = substr;
	                console.log('num', num);
	
	                var length = num.length;
	                var id = pad(num, 6);
	                console.log('id', id);
	
	                dealerId = job;
	                customerKey = dealerId + id;
	                console.log('customerKey', customerKey);
	                
	                // $(this).val(customerKey);
	            } else if (val.length < 6){
	                var length = val.length;
	                var id = pad(val, 6);
	                console.log('id', id);
	
	                customerKey = dealerId + id;
	                console.log('customerKey', customerKey);
	                
	                // $(this).val(customerKey);
	            }
	
	            $.ajax({
	                //url: 'lib/checkPin.php',
	                url: '_inc/_ajx/ajxCustomerActions.php?action=check_pin',
	                data: {
	                    key: customerKey,
	                    job: dealerId
	                },
	                success: function(data, status, resp){
	                    // data = $.parseJSON(data);
	                    console.log('PIN Data', data.result);
	
	                    $this.removeClass('unvalidated');
	
	                    if (data.result){
	                        console.log('Data!!');
	                        var result = data.result;
	                        // $('#promotion').data('needs-created', false);
	                        if (result.Scanned == 'F'){
	                            $('#promotion').data({
	                                'job'               : result.jobs_no,
	                                'customer-key'      : result.CustomerKey,
	                                'needs-created'     : false,
	                                'customer-fname'    : result.CustomerFirstName,
	                                'customer-lname'    : result.CustomerLastName,
	                                'customer-address'  : result.CustomerAddress,
	                                'customer-city'     : result.CustomerCity,
	                                'customer-state'    : result.CustomerState,
	                                'customer-zip'      : result.CustomerZip
	                                // 'customer-scanned'  : result.Scanned
	                            });
	
	                            $this.data('hasData', true);
	                            $this.removeClass('invalid').addClass('valid');
	                            $this.trigger('validated');
	                            
	                            console.log('Validated on line 436');                                    
	                            
	                        } else if(result.Scanned == 'T'){
	                            console.log('Already Scanned!');
	                            // $('#NxtBtn').off('click').on('click', function(e){ e.preventDefault() });
	                            // $('.bubble.help-block').html('<em>This PIN has already been scanned!</em>').css('background', 'red');
	                            // $('.bubble(:after)').css('border-color', 'red transparent');
	                            
	                            // $('#error').msgBox({
	                            //     type: 'error',
	                            //     msg: 'This PIN has already been scanned! Please check to make sure it was entered correctly.'
	                            // });
	                            $validator.showErrors({pin: 'This PIN has already been scanned'});
	
	                            $this.data('hasData', false);
	                            $this.removeClass('valid').addClass('invalid');
	                            $this.trigger('invalidated');
	
	                            console.log('Invalidated on line 447');
	                        }
	                        // $this.data('validator.success', true);
	                    } else {
	                        console.log('!Data');
	                        
	                        $this.data('hasData', false);
	                        
	                        // Throw alert for no PIN
	                        $validator.showErrors({pin: 'That is not a valid PIN'});
	                        
	                        // $('#error').msgBox({
	                        //     type: 'error',
	                        //     msg: 'This PIN does not exist! Please check to make sure it was entered correctly.'
	                        // });
	
	                        $this.removeClass('valid').addClass('invalid');
	                        console.log('Invalidated on line 465');
	                        // $this.
	                        $this.trigger('invalidated');
	                        
	                        // $('#promotion').data('needs-created', true);
	                    }
	
	                    $(document).trigger('recordUpdate');
	                }
	            });
	        }
	    });
	
	    $('#promotion').on({
	        submit: function(e, data)
	        {
	            e.preventDefault();
	            var allData = $(this).data();
	            var $post = {};
	
	            $.each(allData, function(key, value){
	                if ($.type(value) != 'object'){
	                    $post[key] = value;
	                }
	            });
	
	            console.log('Form Submitted');
	            console.log('Form Submitted Data', $post);
	
	            $.ajax({
	                //url: 'lib/formProcessor.php',
	                url: '_inc/_ajx/ajxFormActions.php?action=process_post',
	                data: $post,
	                error: function(request, type, errorThrown){
	                    console.log('Error Data', {
	                        request: request,
	                        type: type,
	                        error: errorThrown
	                    });
	                }
	            }).done(function(data){
	                console.log('Submit Return Data', data);
	            });
	        }
	    });
	    
	    $(document).on({
	        formChange: function(e, data){
	            console.log('data', data);
	            $.extend($('#promotion').data(), data);
	            $('#promotion').mvForm({current: $('#promotion').data('step')});
	
	            if ($('#promotion').data('step') == Number($('fieldset', '#promotion').length - 1)) { // If going to be the last step
	                $('span', '#NxtBtn a').fadeOut(400, function(){
	                    $(this).html('finish').fadeIn(400);
	                });
	
	                $('input', '#step4').each(function(){
	                    console.log("In here");
	                    if ($(this).attr('type') == 'email') {
							$('#promotion').data('email', $(this).val());
							$('#contact-email').val($('#promotion').data('email'));
	                    } else if ($(this).attr('type') == 'tel') {
                            $('#promotion').data('phone', $(this).val());
	                    }                        
	                });
	            } else {
	                if ($('span', '#NxtBtn a').html() == 'finish') {
	                    $('span', '#NxtBtn a').fadeOut(400, function(){
	                        $(this).html('next').fadeIn(400);
	                    });
	                }
	            }
	        },
	        recordUpdate: function(e, data)
	        {
	            console.log('recordUpdate');
	            if (   $('#promotion').data('customer-fname') == ''
	                || $('#promotion').data('customer-lname') == ''
	                || $('#promotion').data('customer-lname') == null
	                || $('#promotion').data('customer-fname') == null
	                || $('#promotion').data('customer-lname') == undefined
	                || $('#promotion').data('customer-fname') == undefined)
	            {
	                console.log('#promotion.data(needs-edit, true)');
	                $('#promotion').data('needs-edit', true);
	                
	                $('#address-correct').parent().removeClass('answer').addClass('hidden-content');
	                $('#edit-address').parent().removeClass('hidden-content').addClass('answer');
	            } else {
	                console.log('#promotion.data(needs-edit, false)');
	                $('#promotion').data('needs-edit', false);
	                
	                $('#address-correct').parent().removeClass('hidden-content').addClass('answer');
	                $('#edit-address').parent().removeClass('answer').addClass('hidden-content');
	            }
	
	            // if ($('#promotion').data('step') == 3) {
	            //     if ($('#promotion').data('needs-edit') == true){
	            //     } else {
	                    
	            //     }
	            // }
	
	            $('address span', '#address-correct').each(function(){
	                var id = $(this).attr('id');
	
	                $(this).html($('#promotion').data(id));
	            });
	
	            $('#edit-address input').each(function(){
	                var id = $(this).attr('id');
	                id = id.substring(5);
	
	                $(this).val($('#promotion').data(id));
	            });
	        },
	        updateCustomer: function(e, data)
	        {
	            console.log('updateCustomer data', data);
	            console.log('Customer Update Started');
	
	            $.ajax({
	                //url: 'lib/updateCustomer.php',
	                url: '_inc/_ajx/ajxFormActions.php?action=update_customer',
	                type: 'json',
	                data: data,
	                async: false,
	                success: function(result){
	                    var parsed = $.parseJSON(result);
	                    // location.reload();
	                    console.log('Final Saved Data Return', parsed);                                
	                },
	                error: function(xhr, status, e){
	                    console.log('Update Error XHR', xhr);
	                    console.log('Update Error Status', status);
	                    console.log('Update Error Object', e);
	                },
	                complete: function(xhr, status){
	                    console.log('Update Complete XHR', xhr);
	                    console.log('Update Complete Status', status);
	                }
	            });
	        },
	        createCustomer: function(e, data)
	        {
	            console.log('Customer create started!');
	            console.log('createCustomer data', data);
	
	            $.ajax({
	                //url: 'lib/createCustomer.php',
	                url: '_inc/_ajx/ajxFormActions.php?action=create_customer',
	                data: data,
	                async: false,
	                success: function(data, status, xhr){
	                    data = $.parseJSON(data);
	                    // location.reload();
	                    console.log('Final Saved Data Return', data);
	                    console.log('Update Success XHR', xhr);
	                    console.log('Update Success Status', status);
	                },
	                error: function(xhr, status, e){
	                    console.log('Update Error XHR', xhr);
	                    console.log('Update Error Status', status);
	                    console.log('Update Error Object', e);
	                },
	                complete: function(xhr, status){
	                    console.log('Update Complete XHR', xhr);
	                    console.log('Update Complete Status', status);
	                }
	            });
	        },
	        updatePrizeLog: function(e, data)
	        {
	            console.log('updatePrizeLog started!');
	            console.log('Update Prize Log data', data);
	
	            $.ajax({
	                //url: 'lib/updatePrizeLog.php',
	                url: '_inc/_ajx/ajxFormActions.php?action=update_prize_log',
	                data: data,
	                async: false,
	                success: function(result){
	                    result = $.parseJSON(result);
	                    console.log('Update Prize Log Return', result);
	                }
	            });
	        },
	        updatePrizes: function(e, data)
	        {
	            console.log('updatePrizes started!');
	            console.log('updatePrizes data', data);
	
	            $.ajax({
	                //url: 'lib/updatePrizes.php',
	                url: '_inc/_ajx/ajxFormActions.php?action=update_prizes',
	                data: data,
	                async: false,
	                success: function(result){
	                    result = $.parseJSON(result);
	                    // location.reload();
	                    console.log('Update Prizes Return', result);
	                }
	            });
	        },
	        formSubmission: function(e, data)
	        {
	            e.preventDefault();
	            var allData = $('#promotion').data();
	
	            console.log('Form Submitted');
	            console.log('Form Submitted Data', allData);
	
	            // $.post('lib/formProcessor.php', allData, function(data, textStatus, xhr) {
	            //     //optional stuff to do after success
	            //     console.log('Submit Return Data', data);
	            // });                    
	
	            // $.ajax({
	            //     url: 'lib/formProcessor.php',
	            //     data: allData,
	            //     type: 'json',
	            //     error: function(request, type, errorThrown){
	            //         console.log('Error Data', {
	            //             request: request,
	            //             type: type,
	            //             error: errorThrown
	            //         });
	            //     }
	            // }).done(function(data){
	            //     console.log('Submit Return Data', data);
	            // });
	        }
	    });
	
	    // $('#save-btn').on({
	    //     click: function(e){
	    //         // e.preventDefault();
	    //         // console.log('Save Started');
	
	    //         // var finish = $.now();
	
	    //         // $('#promotion').data('endTime', finish);
	    //         // console.log('endTime', finish);
	
	    //         // $('#promotion').submit();
	
	    //         // $(document).trigger('formSubmission');
	            
	    //         // if ( $('#promotion').data('needs-created') == true ){
	    //         //     console.log('Creation Started');
	    //         //     var allData = {};
	
	    //         //     $.each($('#promotion').data(), function(k, v){
	    //         //         allData[k] = v;
	    //         //     });
	
	    //         //     console.log('$data', allData);
	
	    //         //     console.log('customerCreate should fire');
	    //         //     $(document).trigger('customerCreate', allData);
	    //         // } else {
	    //         //     var obj = {};
	
	    //         //     $.each($('#promotion').data(), function(k, v){
	    //         //         if (k != 'sp'){
	    //         //             obj[k] = v;
	    //         //         }
	    //         //     });
	
	    //         //     console.log('customerUpdate should fire');
	    //         //     $(document).trigger('customerUpdate', obj);
	    //         //     // $(document).trigger('updatePrizeLog', obj);
	    //         // }
	
	    //         // if ( $('#promotion').data('isWinner') == true ){ // Winner!!
	    //         //     console.log('Winner Prize Update Started');
	    //         //     var allData = {};
	
	    //         //     $.each($('#promotion').data(), function(k, v){
	    //         //         allData[k] = v;
	    //         //     });
	
	    //         //     console.log('$data', allData);
	
	    //         //     $(document).trigger('updatePrizes', allData);
	    //         // } else { // No Winner -OR- Default Prize Winner
	    //         //     console.log('Prize Log Update Started');
	    //         //     var allData = {};
	
	    //         //     $.each($('#promotion').data(), function(k, v){
	    //         //         allData[k] = v;
	    //         //     });
	
	    //         //     console.log('$data', allData);
	
	    //         //     $(document).trigger('updatePrizeLog', allData);
	    //         // }
	    //     }
	    // });
	
	    $('input[type="tel"]').on({
	        focus: function(e){
	            $(this).mask("(999) 999-9999");
	        }
	    });
	
	    $('#restart-btn').on('click', function(e, data){
	        e.preventDefault();
	        location.reload();
	    });
	
	    $('#salesperson').selectBoxIt({
	        showEffect          : 'fadeIn',
	        showEffectSpeed     : 400,
	        hideEffect          : 'fadeOut',
	        hideEffectSpeed     : 400,
	        aggressiveChange    : true,
	        showFirstOption     : false
	    }).on({
	        click: function(e)
	        {
	            console.log('Click Event Fired!');
	        },
	        change: function(e)
	        {
	            console.log('Change Event Firing!');
	            var selectBox = $(this).data('selectBox-selectBoxIt');
	            var $select = $('#' + $(this).attr('id') + 'SelectBoxIt');
	
	            var val = $(this).val();
	            var names = val.split(' ');
	            var firstName = names[0];
	            var lastName = names[1];
	            var sp = {
	                'sp-fname': firstName,
	                'sp-lname': lastName,
	                'sp-id'   : $(this).data('sp-id')
	            };
	
	            var valid = $(this).valid();
	
	            $(this).removeClass('unvalidated');
	            $select.removeClass('unvalidated');
	            
	            if (valid){
	                $(this).removeClass('invalid').addClass('valid');
	                $select.removeClass('invalid').addClass('valid');
	                selectBox.refresh();
	            } else {
	                $(this).removeClass('valid').addClass('invalid');
	                $select.removeClass('valid').addClass('invalid');
	                selectBox.refresh();
	            }
	
	            $('#promotion').data('sp-fname', firstName).data('sp-lname', lastName).data('sp-id', $(this).data('sp-id'));
	
	            clearTimeout($timer);
	            $timer = setTimeout(function(){
	                console.log('Auto-progress fired');
	                
	                $('#NxtBtn').click();
	            }, 250);
	
	            $('#promotion').data('timer', $timer);
	        }
	    });
	
	    $('.no-pin-btn').on('click', function(e){
	        $('#promotion').data('needs-edit', true);
	        $('#pin').rules('add', {
	            required: false
	        });
	
	        $(document).trigger('recordUpdate');
	        $('#NxtBtn').click();
	    });
	
	    $('.icon-btn.add-one', '#step2').on('click', function(e){
	        var div = $('.content-div.answer', '#step2');
	        var newContent = $('.content-div.hidden-content', '#step2');
	        var original = $('#sp-select').html();
	        var fieldset = $('#sp-select');
	        // var newContent = $('.content-div.hidden-content', '#step2').html();
	
	        $('#salesperson').rules('add', {
	            required: false
	            // valueNotEquals: ''
	        });
	
	        $("input[name=sp-fname], input[name=sp-lname], input[name=sp-email]").each(function(){
	            $(this).rules('add', {
	                required: true
	            });
	        });
	
	        console.log('step3');
	        div.fadeOut(400, function(){
	            newContent.fadeIn(400).css('display', 'table-cell');
	        });
	
	        if (newContent.css('display') == 'none'){
	            newContent.data('shown', false);
	        } else {
	            newContent.data('shown', true);
	        }
	        // div.fadeOut(400, function(){
	        //     fieldset.html(newContent);
	        // }).fadeIn(400, function(){
	        //     $('.x-icon', '#step2').on('click', function(e){
	        //         // // console.log('original content', original);
	        //         div.fadeOut(400, function(){
	        //             fieldset.html(original);
	        //         }).fadeIn(400);
	        //     });
	
	        //     $('.checkmark', '#step2').on('click', function(e){
	        //         e.preventDefault();
	        //         $('#NxtBtn').click();
	        //         fieldset.fadeOut(400, function(){
	        //             $(this).html(original).fadeIn(400);
	        //         });
	        //     });
	        // });
	    });
	
	    $('.icon-btn.checkmark', '#step2').on({
	        click: function(e) {
	            var sp = {};
	
	            $('input', '#sp-entry-form').each(function(){
	                var value = $(this).val();
	                var field = $(this).attr('name');
	                console.log(field);
	
	                sp[field] = value;
	            });
	
	            console.log('sp data', sp);
	
	            $.extend($('#promotion').data(), sp);
	
	            $.ajax({
	                //url: 'lib/createSp.php',
	                url: '_inc/_ajx/ajxDealerActions.php?action=create_salesperson',
	                data: sp,
	                success: function(data){
	                    console.log( 'CreateSP Return', data );
	                    //data = $.parseJSON(data);							
	                    var result = $('<span class="result-msg success"/>').html('You have been successfully added!');
	
	                    // $('.buttons-row').append(result);
	                    $('#sp-entry-form').fadeOut(400, function(){
	                        $(this).append(result).fadeIn(400, function(){
	                            $('#NxtBtn').click();
	                        });
	                    });
	                    console.log('SalepersonID', data.SalepersonID);
	                    $('#promotion').data('sp-id', data.SalepersonID);
	                }
	            });
	        }
	    });
	
	    $('.x-icon', '#step2').on({
	        click: function(e) {
	            $("#salesperson").rules('add', {
	                required: true
	            });
	
	            $("input[name=sp-fname], input[name=sp-lname], input[name=sp-email]").each(function(){
	                $(this).rules('add', {
	                    required: false
	                });
	            });
	            $('.content-div.hidden-content', '#step2').fadeOut(400, function(){
	                $('.content-div.answer', '#step2').fadeIn(400);
	            });
	        }
	    });
	
	    $('.icon-btn.x-icon', '#sp-entry').on({
	        click: function(e) {
	            $('input', 'fieldset#sp-entry').each(function(key, input){
	                $(input).val('');
	            });
	        }
	    });
	
	    $('.icon-btn.checkmark', 'fieldset#address-correct').on({
	        click: function(e) {
	            $("#edit-customer-fname, #edit-customer-lname, #edit-customer-address, #edit-customer-city, #edit-customer-state, #edit-customer-zip").each(function(){
	                $(this).val('');
	                $(this).rules('add', {
	                    required: false,
	                });
	            });
	
	            $('#NxtBtn').click();
	            // $('.buttons-row').append(result);
	            // $('#sp-entry-form').append(result).fadeIn(400, function(){
	            //     $('#NxtBtn').fadeIn(300);
	            // });
	        }
	    });
	
	    $('.icon-btn.x-icon', '#address-correct').on({
	        click: function(e) {
	            $('#address-correct', '#step3').parent().fadeOut(400, function(){
	                $('#edit-address', '#step3').parent().fadeIn(400);
	            });
	
	            // $('input', '#edit-address').each(function(){
	            //     $(this).rules('add', {
	            //         required: true
	            //     });
	            // });
	
	            $("#edit-customer-fname, #edit-customer-lname, #edit-customer-address, #edit-customer-city, #edit-customer-state, #edit-customer-zip").each(function(){
	                $(this).rules('add', {
	                    required: true,
	                });
	            });
	            // $('#NxtBtn').fadeOut(300);
	        }
	    });
	
	    $('.icon-btn.x-icon', '#edit-address').on({
	        click: function(e) {
	            // $('input', 'fieldset#edit-address').each(function(key, input){
	            //     $(input).val('');
	
	            //     $(this).rules('add', {
	            //         required: false
	            //     });
	            // });
	            
	            $("#edit-customer-fname, #edit-customer-lname, #edit-customer-address, #edit-customer-city, #edit-customer-state, #edit-customer-zip").each(function(){
	                $(this).val('');
	                $(this).rules('add', {
	                    required: false,
	                });
	            });
	
	            $('#address-correct', '#step3').parent().fadeOut(400, function(){
	                $('#edit-address', '#step3').parent().fadeIn(400);
	            });
	
	            // $('#NxtBtn').fadeIn(300);
	        }
	    });
	
	    $('.icon-btn.checkmark', 'fieldset#edit-address').on({
	        click: function(e) {
	            var sp = {};
	
	            $('input', 'fieldset#edit-address').each(function(key, input){
	                var value = $(input).val();
	                var field = $(input).attr('name');
	
	                sp[field] = value;
	            });
	
	            $.extend($('#promotion').data(), sp);
	            
	            var result = $('<span class="result-msg success"/>').html('You have successfully added your info!');
	            // $('.buttons-row').append(result);
	            $('.hidden-content', '#step3').append(result);
	
	            setTimeout(function(){
	                $('#NxtBtn').click();
	                $('#NxtBtn').fadeIn(300);
	                // $(document).trigger('formChange', $('#promotion').data());
	            }, 400);
	        }
	    });
	
	    $('.answer .icon-btn.x-icon', '#step3').on({
	        click: function(e) {
	            $('.content-div.answer', '#step3').fadeOut(400, function(){
	                $('.content-div.hidden-content', '#step3').fadeIn(400).css('display', 'table-cell');
	                // $('#NxtBtn').addClass('inactive');
	            });
	        }
	    });
	
	    $('.hidden-content .icon-btn.x-icon', '#step3').on({
	        click: function(e) {
	            $('.content-div.hidden-content', '#step3').fadeOut(400, function(){
	                $('.content-div.answer', '#step3').fadeIn(400).css('display', 'table-cell');
	                // $('#NxtBtn').addClass('inactive');
	            });   
	        }
	    });
	
	    $('select', '#step5').each(function(){
	        var input = $('<input type="hidden" />').attr('id', $(this).attr('id') + '-value');
	        $('#trade-value').append(input);
	
	        $(this).selectBoxIt({
	            showEffect          : 'fadeIn',
	            showEffectSpeed     : 400,
	            hideEffect          : 'fadeOut',
	            hideEffectSpeed     : 400,
	            aggressiveChange    : true,
	            // theme               : 'bootstrap',
	            showFirstOption     : false
	        }).on({
	            change: function(e) {
	                // console.log('Value', $(this).val());
	                // console.log('ID', $(this).attr('id'));
	                var id = $(this).attr('id');
	                var val = $(this).val();
	
	                // $(input).val($(this).val());
	                $('#promotion').data(id, val);
	            
	                var selectBox = $(this).data('selectBox-selectBoxIt');
	                var $select = $('#' + $(this).attr('id') + 'SelectBoxIt');
	                var valid = $(this).valid();
	
	                $(this).removeClass('unvalidated');
	                $select.removeClass('unvalidated');
	                
	                if (valid){
	                    $(this).removeClass('invalid').addClass('valid');
	                    $select.removeClass('invalid').addClass('valid');
	                    selectBox.refresh();
	                } else {
	                    $(this).removeClass('valid').addClass('invalid');
	                    $select.removeClass('valid').addClass('invalid');
	                    selectBox.refresh();
	                }
	
	                if ($(this).is(':last-child')){
	                    clearTimeout($timer);
	                    $timer = setTimeout(function(){
	                        console.log('Auto-progress fired');
	                        
	                        $('#NxtBtn').click();
	                    }, 250);
	
	                    $('#promotion').data('timer', $timer);
	                }
	            }
	        });
	    });
	
	    $('.skip-btn', '#step5').on({
	        click: function(e) {
	            $('#NxtBtn').click();
	        }
	    });
	
	/*//////////////////////////// ORIGINAL CH-CH-CHANGES CODE START ////////////////////////////
	
	    $('.icon', '#step6 .choose-one').on({
	        click: function(e) {
	            $('.icon', '#step6 .choose-one').removeClass('action');
	            $(this).addClass('action');
	
	            if ($('#when-wrap').css('display') == 'none') {
	                $('#when-wrap').fadeIn(400).show();
	            }
	
	            $('#condition').val($('span', this).html());
	        }
	    });
	
	//////////////////////////// ORIGINAL CH-CH-CHANGES CODE END ////////////////////////////*/
	
	/*//////////////////TWEAKED CH-CH-CHANGES CODE START////////////////// */
	
	    $('.kiosk-select', '#step5 .input-wrap').on({
	        click: function(e) {
	            $('.kiosk-select', '#step6 ').removeClass('action');
	            $(this).addClass('action');
	
	            if ($('#when-wrap').css('display') == 'none') {
	                $('#when-wrap').fadeIn(1200).show();
	            }
	
	            $('#condition').val($('span', this).html());
	        }
	    });
	    
	    $('.icon', '#step5 .input-wrap').on({
	        click: function(e) {
	            $('.icon', '#step5 ').removeClass('action');
	            $(this).addClass('action');
	
	            $('#condition').val($('span', this).html());
	
	            if ( $('#condition').val().length > 0 ){
	                clearTimeout($timer);
	                $timer = setTimeout(function(){
	                    console.log('Auto-progress fired');
	                    
	                    $('#NxtBtn').click();
	                }, 250);
	
	                $('#promotion').data('timer', $timer);
	            }
	        }
	    });
	
	/*//////////////////TWEAKED CH-CH-CHANGES CODE END////////////////// */
	
	    $('#when', '#step5').selectBoxIt({
	        showEffect          : 'fadeIn',
	        showEffectSpeed     : 400,
	        hideEffect          : 'fadeOut',
	        hideEffectSpeed     : 400,
	        aggressiveChange    : true,
	        // theme               : 'bootstrap',
	        showFirstOption     : false
	    }).on({
	        change: function(e) {
	            // // console.log('Value', $(this).val());
	            // // console.log('ID', $(this).attr('id'));
	            $('#timeframe').val($(this).val());
	
	            var selectBox = $(this).data('selectBox-selectBoxIt');
	            var $select = $('#' + $(this).attr('id') + 'SelectBoxIt');
	            var valid = $(this).valid();
	
	            $(this).removeClass('unvalidated');
	            $select.removeClass('unvalidated');
	            
	            if (valid){
	                $(this).removeClass('invalid').addClass('valid');
	                $select.removeClass('invalid').addClass('valid');
	                selectBox.refresh();
	            } else {
	                $(this).removeClass('valid').addClass('invalid');
	                $select.removeClass('valid').addClass('invalid');
	                selectBox.refresh();
	            }
	        }
	    });
	
	    $('#mechanic-select').selectBoxIt({
	        showEffect          : 'fadeIn',
	        showEffectSpeed     : 400,
	        hideEffect          : 'fadeOut',
	        hideEffectSpeed     : 400,
	        aggressiveChange    : true,
	        showFirstOption     : false
	    }).on({
	        change: function(e) {
	            $('#mechanic').val($(this).val());
	
	            var selectBox = $(this).data('selectBox-selectBoxIt');
	            var $select = $('#' + $(this).attr('id') + 'SelectBoxIt');
	            var valid = $(this).valid();
	
	            $(this).removeClass('unvalidated');
	            $select.removeClass('unvalidated');
	            
	            if (valid){
	                $(this).removeClass('invalid').addClass('valid');
	                $select.removeClass('invalid').addClass('valid');
	                selectBox.refresh();
	            } else {
	                $(this).removeClass('valid').addClass('invalid');
	                $select.removeClass('valid').addClass('invalid');
	                selectBox.refresh();
	            }
	
	            clearTimeout($timer);
	            $timer = setTimeout(function(){
	                console.log('Auto-progress fired');
	                
	                $('#NxtBtn').click();
	            }, 250);
	
	            $('#promotion').data('timer', $timer);
	        }
	    });
	
	    $('.icon-btn', '#buzz-table').on({
	        click: function(e){
	            if ($(this).hasClass('selected')) {
	                $(this).removeClass('selected');
	            } else {
	                $(this).addClass('selected');
	            }
	        }
	    });
	
	    $('.selectboxit-container', '#trade-value').each(function(){
	        var parent = $(this).parent();
	        
	        // // // console.log.log('parent', parent);
	        // // // console.log.log('parent select', $('select', parent));
	
	        $('select', parent).each(function(){
	            var nextParent = parent.next();
	            var next = $('span.cf-select', nextParent);
	
	            $(this).on({
	                change: function(e){
	                    var selectBox = $(this).data('selectBox-selectBoxIt');
	                    var $select = $('#' + $(this).attr('id') + 'SelectBoxIt');
	                    var valid = $(this).valid();
	
	                    $(this).removeClass('unvalidated');
	                    $select.removeClass('unvalidated');
	                    
	                    if (valid){
	                        $(this).removeClass('invalid').addClass('valid');
	                        $select.removeClass('invalid').addClass('valid');
	                        selectBox.refresh();
	                    } else {
	                        $(this).removeClass('valid').addClass('invalid');
	                        $select.removeClass('valid').addClass('invalid');
	                        selectBox.refresh();
	                    }
	
	                    next.removeClass('inactive');
	
	                    if ($(this).is(':last-child')){
	                        clearTimeout($timer);
	                        $timer = setTimeout(function(){
	                            console.log('Auto-progress fired');
	                            
	                            $('#NxtBtn').click();
	                        }, 250);
	
	                        $('#promotion').data('timer', $timer);
	                    }
	                }
	            });
	        });
	    });
	
	    $('#year').on({
	        change: function(e){
	            var $sel = $('#' + $(this).attr('id') + 'SelectBoxIt');
	
	            $(this).removeClass('unvalidated');
	            $sel.removeClass('unvalidated');
	            $(this).data('selectBox-selectBoxIt').refresh();
	            
	            fillMakeSelect();
	            // // console.log('year', $('#year').val());
	        }
	    });
	
	    $('#make').on({
	        change: function(e){
	            var $sel = $('#' + $(this).attr('id') + 'SelectBoxIt');
	            
	            $(this).removeClass('unvalidated');
	            $sel.removeClass('unvalidated');
	            $(this).data('selectBox-selectBoxIt').refresh();
	            
	            fillModelSelect();
	        }
	    });
	
	    $('#model').on({
	        change: function(e){
	            var $sel = $('#' + $(this).attr('id') + 'SelectBoxIt');
	            
	            $(this).removeClass('unvalidated');
	            $sel.removeClass('unvalidated');
	            $(this).data('selectBox-selectBoxIt').refresh();
	        }
	    });
	
	    $('#mileage').on({
	        change: function(e){
	            var $sel = $('#' + $(this).attr('id') + 'SelectBoxIt');
	            
	            $(this).removeClass('unvalidated');
	            $sel.removeClass('unvalidated');
	            $(this).data('selectBox-selectBoxIt').refresh();
	
	            clearTimeout($timer);
	            $timer = setTimeout(function(){
	                console.log('Auto-progress fired');
	                
	                $('#NxtBtn').click();
	            }, 250);
	
	            $('#promotion').data('timer', $timer);
	        }
	    });
	
	    var fillMakeSelect = function(){
	        var year = $('#year').val();
	        var make = $('#make');
	
	        if (year != null & year != '') {
	          $.ajax({
	            //url: 'lib/getMakes.php',
	            url: '_inc/_ajx/ajxDealerActions.php?action=get_makes',
	            data: {
	              year: year
	            }
	          }).done(function(data){
	            // // // console.log.log('data', data);
	            
	            // Reset Model Options
	            make.html('<option>MAKE</option>');
	            make.removeClass('inactive');
	
	            $(data).each(function(){
	              var option = $('<option/>');
	              
	              // // // console.log.log('this', this);
	
	              option.val(this).html(this);
	              make.append(option);
	            });
	
	            if (year != 'YEAR' & year != null){
	              var selectBox = $('#make').data('selectBox-selectBoxIt');
	              selectBox.refresh();
	            }
	          });
	        }
	    }
	
	    var fillModelSelect = function(){
	        var make = $('#make').val();
	        var year = $('#year').val();
	        var model = $('#model');
	
	        if (make != null && make != '' && year != null & year != '') {
	            var choices = $('option', model);
	
	            // if (choices.length <= 1) {
	                $.ajax({
	                    //url: 'lib/getModels.php',
	                    url: '_inc/_ajx/ajxDealerActions.php?action=get_models',
	                    data: {
	                        year: year,
	                        make: make
	                    }
	                }).done(function(data){
	                    // // // console.log.log('data', data);
	                    
	                    // Reset Model Options
	                    model.html('<option>MODEL</option>').removeClass('inactive');
	
	                    $(data).each(function(){
	                        var option = $('<option/>');
	                        
	                        // // // console.log.log('this', this);
	
	                        option.val(this).html(this);
	                        model.append(option);
	                    });
	
	                    if (make != 'MAKE' && make != null && year != 'YEAR' & year != null){
	                        var selectBox = $('#model').data('selectBox-selectBoxIt');
	
	                        selectBox.refresh();
	                    }
	                });
	            // }
	        }
	    }
	
	    var pad = function(str, max){
	        return str.length < max ? pad("0" + str, max) : str;
	    }
	});
</script>