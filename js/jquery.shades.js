(function($){
	var methods = {
		init: function(options)
		{
			return $.shades('shadeColor', options);
		},
		shadeColor: function(options)
		{
			var color = options.color;
			var percent = options.percent;

			var R = parseInt(color.substring(1,3),16);
		    var G = parseInt(color.substring(3,5),16);
		    var B = parseInt(color.substring(5,7),16);

		    R = parseInt(R * (100 + percent) / 100);
		    G = parseInt(G * (100 + percent) / 100);
		    B = parseInt(B * (100 + percent) / 100);

		    R = (R<255)?R:255;  
		    G = (G<255)?G:255;  
		    B = (B<255)?B:255;  

		    var RR = ((R.toString(16).length==1)?"0"+R.toString(16):R.toString(16));
		    var GG = ((G.toString(16).length==1)?"0"+G.toString(16):G.toString(16));
		    var BB = ((B.toString(16).length==1)?"0"+B.toString(16):B.toString(16));

		    return "#"+RR+GG+BB;
		},
		rgb2hex: function(rgb) {
		    rgb = rgb.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
		    
		    function hex(x) {
		        return ("0" + parseInt(x).toString(16)).slice(-2);
		    }

		    return "#" + hex(rgb[1]) + hex(rgb[2]) + hex(rgb[3]);
		}
	};

	$.fn.shades = function(method) {
		if (methods[method]) {
			return methods[method].apply(this, Array.prototype.slice.call( arguments, 1 ));
		} else if (typeof method === 'object' || !method) {
			return methods.init.apply(this, arguments);
		} else {
			$.error('Method ' +  method + ' does not exist on jQuery.shades');
		}
	}
})(jQuery);