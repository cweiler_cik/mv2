(function($) {
	// Shell for your plugin code
	$.fn.initBg = function(options, callback)
	{
		if ($.isFunction(options)) {
			callback = options;
			options = null;
		}

		options = $.extend($.fn.initBg.defaults, options);

		return this.each(function(dest) {
			var img = $('<img id="bg-img"/>');

			$(img).attr('src', options.src).attr('alt', options.alt).attr('title', options.title);
			$(this).append(img);

			if (typeof callback == 'function'){
				callback.call(this);
			}
		});
	}

	$.fn.initBg.defaults = {
		src: 'img/landscape.jpg',
		alt: null,
		title: null
	};
})(jQuery);

(function($) {
	$.fn.promoNav = function(formData, callback) {
		if ($.isFunction(formData)) {
			callback = formData;
			formData = null;
		}

		return this.each(function() {
			var a = $('a', this);

			console.log('promoNav.formData', formData);
			console.log('anchor', $('a', this));

			if ($('a', this).attr('rel') == 'next') {
				$('#content form').kioskForm('next', {'url': $('#next-btn').attr('href'), 'formData': formData});
				// var callback = function() {
				// 	$('#reload').nav('next', {'url': $('#next-btn').attr('href'), 'formData': formData});
				// }

				// $('#content form').kioskForm('next', {'url': $('#next-btn').attr('href'), 'formData': formData, 'callback': callback});

			} else if ($('a', this).attr('rel') == 'back') {
				$('#content form').kioskForm('back', {'url': $('#back-btn').attr('href'), 'formData': formData});
				// var callback = function() {
				// 	$('#reload').nav('back', {'url': $('#back-btn').attr('href'), 'formData': formData});
				// }
				
				// $('#content form').kioskForm('back', {'url': $('#back-btn').attr('href'), 'formData': formData, 'callback': callback});
			}

			if (typeof callback == 'function'){
				callback.call(this);
			}
		});
	};
})(jQuery);

(function($) {
	var methods = {
		submit: function(formData)
		{
			var obj = {};
			console.log('Form', $('#content form'));
			 
			if ($('#content form') != undefined && $('#content form') != null) {
				var processor = 'tools/formProcessing.php';

				$('#content form input').each(function(id, value){
					var name = $(this).attr('name');
					var val = $(this).val();

					obj[name] = val;
				});

				$('#reload').load(processor, obj, function(response, status, xhr){
					if (status == 'error') {
						var msg = "Sorry but there was an error: ";
	    				var error = $('<div id="error" class="alert alert-error"/>').html($('<button type="button" class="close" data-dismiss="alert"/>').html('&times;') + msg + xhr.status + " " + xhr.statusText);
	    				// alert(error);
	    				return error;
					}
				});
			} else {
				// Handle data from the game
				obj['wheel'] = 'success';
			}

			return obj;
		},
		next: function(options)
		{
			return this.each(function() {
				$('#reload .container').slide('out');

				var form = options.formData;
				var link = options.url;
				var data = $(this).kioskForm('submit', form);
				var callback = options.callback;
				var formData = {};

				$.extend(formData, form);
				$.extend(formData, data);

				console.log('PreNav Form Data', formData);

				$('#reload').trigger('loadNext', {'url': link, 'params': formData});

				if (typeof callback == 'function') {
					callback.call(this);
				}

				// console.log('formData', formData);
			});
		},	
		back: function(options)
		{
			return this.each(function() {
				var form = options.formData;
				var link = options.url;
				var callback = options.callback;
				var formData = {};

				$.extend(formData, form);
				// $.extend(formData, data);

				// console.log('PreNav Form Data', formData);

				$('#reload').trigger('loadPrev', {'url': link, 'params': formData});

				if (typeof callback == 'function') {
					callback.call(this);
				}
			});
		}
	};

	$.fn.kioskForm = function(method) {
		if (methods[method]) {
			return methods[method].apply(this, Array.prototype.slice.call( arguments, 1 ));
		} else if (typeof method === 'object' || !method) {
			return methods.init.apply(this, arguments);
		} else {
			$.error('Method ' +  method + ' does not exist on jQuery.kioskForm');
		}
	};
})(jQuery);

(function($) {
	var methods = {
		next: function(options)
		{
			var link = options.url;
			var params = options.params;

		  	return this.each(function() {
		  		$(this).load(link, params);
		  	});
		},	
		back: function(options)
		{
			var link = options.url;
			var params = options.params;

			return this.each(function() {				
				$(this).load(link, params);
			});
		}
	}

	$.fn.nav = function(method) {
		if (methods[method]) {
			return methods[method].apply(this, Array.prototype.slice.call( arguments, 1 ));
		} else if (typeof method === 'object' || !method) {
			return methods.init.apply(this, arguments);
		} else {
			$.error('Method ' +  method + ' does not exist on jQuery.nav');
		}    
	};
})(jQuery);

(function($) {
	var methods = {
		in: function(callback)
		{
			return this.each(function() {
				var startPos = $('#reload').position();
				var midLeft = Number($(window).width()/2 - $(this).outerWidth()/2);
				
				// console.log('startPos', startPos);
				// console.log('midLeft', midLeft);

				$(this).css({
			        'position': 'absolute',
			        'left': window.outerWidth,
			        'top': startPos.top
			    });

			    $(this).css('display', 'block');

			    $(this).animate({
			        'left': midLeft
			    }, 800);

			    if (typeof callback == 'function'){
					callback.call(this);
				}
			});
		},
		out: function(callback)
		{
			return this.each(function() {
				// $(this).hide('slow');
				var startPos = $('#reload').position();
				var leftPos = Number(-1 * $(this).outerWidth()) + 'px';
				console.log('leftPos', leftPos);

			    $(this).animate({
			        'left': leftPos
			    });

			    if (typeof callback == 'function'){
					callback.call(this);
				}
			});
		}
	};

	$.fn.slide = function(method) {
		if (methods[method]) {
			return methods[method].apply(this, Array.prototype.slice.call( arguments, 1 ));
		} else if (typeof method === 'object' || !method) {
			return methods.init.apply(this, arguments);
		} else {
			$.error('Method ' +  method + ' does not exist on jQuery.slide');
		}
	}
})(jQuery);