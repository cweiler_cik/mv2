(function($){
	var dataObj = {};
	var methods = {
		initPage: function(options)
		{
			if ($.isFunction(options)) {
				callback = options;
				options = null;
			} else {
				callback = options.complete;
			}

			return this.each(function(){
				if (options !== null && options !== undefined && options != '') {
					var step = options.step;
					var params = options;
				} else {
					var step, params;
				}

				console.log({
					'step'		: step,
					'params'	: params,
				});

				$('form', '#reload').link(dataObj).find('input[type=text], input[type=hidden]').trigger('change');
				$.extend($('#sessData').data(), params);

				if (step == undefined || step == null || step == '') {
					step = 'init';
				}

				console.log('Current Step', step);

				if (step != 'init' && step != 0) { // Not the first step
					$('#BckBtn').show();
					$(this).kiosk('initNav', params);
				} else {
					$.ajax({
						url: 'content/step1.php',
						data: params,
						type: 'POST',
						context: $('#reload'),
						dataType: 'text'
					}).done(function(response, status){
						console.log({
							response: response,
							status: status
						});

						$(this).html(response);
						$(this).kiosk('initNav', params);
					});
				}

				// console.log('Step', step);

				if (typeof callback == 'function'){
					callback.call(this);
				}
			});
		},
		initNav: function(options)
		{
			var formData = options;
			var step = options.step;
			var next = Number(step + 1);
			var back = Number(step - 1);
			var baseUrl = '/kiosks-new/';

			console.log({
				'step': step,
				'next': next,
				'back': back
			});

			$('#BckBtn a').attr('href', baseUrl + 'content/step' + back + '.php');
			$('#NxtBtn a').attr('href', baseUrl + 'content/step' + next + '.php');

			// console.log('JSON Data', formData); // Same as $_POST
			// console.log('Next Step', next);
			// console.log('Prev Step', back);

			$('#content form input').each(function(){
				$(this).on({
					keypress: function(event){
						if(event.keyCode == 13) {
							event.preventDefault();
							$('#NxtBtn').click();
						}
					},
				});
			});

			$('#BckBtn').on({
				click: function(event){
					event.preventDefault();
					var obj = {};

					$.extend(formData, {step: back});
					
					// var data = $(this).kiosk('submit', formData);
					
					// $.extend(formData, data);
					// $.extend(obj, formData);

					// var params = obj;

					// console.log('InitNav Params', params);
					$('#reload').trigger('navBack', formData);
				}
			});

			$('#NxtBtn').on({
				click: function(event){
					event.preventDefault();
					var obj = {};

					$.extend(formData, {step: next});
					
					// var data = $(this).kiosk('submit', formData);
					
					// $.extend(formData, data);
					// $.extend(obj, formData);

					// var params = obj;

					// console.log('InitNav Params', params);
					$('#reload').trigger('navNext', formData);
				}
			});
		},
		shadeColor: function(options)
		{
			var color = options.color;
			var percent = options.percent;

			var R = parseInt(color.substring(1,3),16);
		    var G = parseInt(color.substring(3,5),16);
		    var B = parseInt(color.substring(5,7),16);

		    R = parseInt(R * (100 + percent) / 100);
		    G = parseInt(G * (100 + percent) / 100);
		    B = parseInt(B * (100 + percent) / 100);

		    R = (R<255)?R:255;  
		    G = (G<255)?G:255;  
		    B = (B<255)?B:255;  

		    var RR = ((R.toString(16).length==1)?"0"+R.toString(16):R.toString(16));
		    var GG = ((G.toString(16).length==1)?"0"+G.toString(16):G.toString(16));
		    var BB = ((B.toString(16).length==1)?"0"+B.toString(16):B.toString(16));

		    return "#"+RR+GG+BB;
		},
		rgb2hex: function(rgb) {
		    rgb = rgb.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
		    
		    function hex(x) {
		        return ("0" + parseInt(x).toString(16)).slice(-2);
		    }

		    return "#" + hex(rgb[1]) + hex(rgb[2]) + hex(rgb[3]);
		}
	};

	$.fn.kiosk = function(method) {
		if (methods[method]) {
			return methods[method].apply(this, Array.prototype.slice.call( arguments, 1 ));
		} else if (typeof method === 'object' || !method) {
			return methods.init.apply(this, arguments);
		} else {
			$.error('Method ' +  method + ' does not exist on jQuery.kiosk');
		}
	}
})(jQuery);