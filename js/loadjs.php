<!-- Le javascript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="js/jquery.js"></script>    
<script src="js/jquery-ui.min.js"></script>
<script src="js/jquery.selectBoxIt.min.js"></script>
<script src="js/bootstrap-transition.js"></script>
<script src="js/bootstrap-alert.js"></script>
<script src="js/bootstrap-modal.js"></script>
<!-- <script src="js/bootstrap-dropdown.js"></script> -->

<!--
<script src="js/bootstrap-scrollspy.js"></script>
<script src="js/bootstrap-tab.js"></script>
 -->

<script src="js/bootstrap-tooltip.js"></script>
<script src="js/bootstrap-popover.js"></script>
<script src="js/bootstrap-button.js"></script>
<script src="js/bootstrap-collapse.js"></script>
<!-- // <script src="js/bootstrap-carousel.js"></script> -->
<!-- // <script src="js/bootstrap-typeahead.js"></script> -->
<script src="js/jquery.maskedinput.min.js"></script>
<script src="js/jquery.validate.min.js"></script>
<script src="js/additional-methods.min.js"></script>
<script src="js/jquery.mv.js"></script>
<script src="js/jquery.msgBox.js"></script>
<!-- <script src="js/cik.prototype.js"></script> -->

<script src="../_tools/globals/_js/general.js"></script>
<script src="../_tools/globals/_js/jquery-carouFredSel/jquery.carouFredSel-6.2.0-packed.js"></script>
<script src="../_tools/globals/_js/jquery-touchSwipe/jquery.touchSwipe.min.js"></script>
<script src="../_tools/globals/_js/jquery-easing/jquery.easing.min.js"></script>
<script src="../_tools/globals/_js/jquery-sfx/sfx.min.js"></script>
<?php include("spin2win/_js/vars.php"); ?>

<?php include('js/jsmain.php'); ?>

<script src="spin2win/_js/vertical_kiosk.js"></script>

<script type="text/javascript">
    $(document).ready(function() {       
        window.setInterval(dataSend, 60000); //1 minute
    });

    function dataSend(){        
        $.ajax({
            //'url': 'lib/data-sender.php',
            'url': '_fns/data-sender.php',
            'type': 'POST'
        });
    }
</script>