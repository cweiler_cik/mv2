(function($){
	var dataObj = {};
	var methods = {
		init: function(options)
		{
			if ($.isFunction(options)) {
				callback = options;
				options = null;
			} else {
				callback = options.complete;
			}

			return this.each(function(){

				$('form', '#reload').link(dataObj).find('input[type=text], input[type=hidden]').trigger('change');
				$.extend($('#sessData').data(), params);

				$.ajax({
					url: 'content/step1.php',
					data: params,
					type: 'POST',
					context: $('#reload'),
					dataType: 'text'
				}).done(function(response, status){
					console.log({
						response: response,
						status: status
					});

					$(this).html(response);
					$(this).kiosk('initNav', params);
				});
			});
		},
		shadeColor: function(options)
		{
			var color = options.color;
			var percent = options.percent;

			var R = parseInt(color.substring(1,3),16);
		    var G = parseInt(color.substring(3,5),16);
		    var B = parseInt(color.substring(5,7),16);

		    R = parseInt(R * (100 + percent) / 100);
		    G = parseInt(G * (100 + percent) / 100);
		    B = parseInt(B * (100 + percent) / 100);

		    R = (R<255)?R:255;  
		    G = (G<255)?G:255;  
		    B = (B<255)?B:255;  

		    var RR = ((R.toString(16).length==1)?"0"+R.toString(16):R.toString(16));
		    var GG = ((G.toString(16).length==1)?"0"+G.toString(16):G.toString(16));
		    var BB = ((B.toString(16).length==1)?"0"+B.toString(16):B.toString(16));

		    return "#"+RR+GG+BB;
		},
		rgb2hex: function(rgb) {
		    rgb = rgb.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
		    
		    function hex(x) {
		        return ("0" + parseInt(x).toString(16)).slice(-2);
		    }

		    return "#" + hex(rgb[1]) + hex(rgb[2]) + hex(rgb[3]);
		}
	};

	$.fn.kiosk = function(method) {
		if (methods[method]) {
			return methods[method].apply(this, Array.prototype.slice.call( arguments, 1 ));
		} else if (typeof method === 'object' || !method) {
			return methods.init.apply(this, arguments);
		} else {
			$.error('Method ' +  method + ' does not exist on jQuery.kiosk');
		}
	}
})(jQuery);

(function($){
	$.fn.mvForm = function(options)	{
		if ($.isFunction(options)) {
			callback = options;
			options = null;
		} else {
			options = $.extend($.fn.mvForm.defaults, options);
			callback = options.complete;
		}

		options = $.extend($.fn.mvForm.defaults, options);

		return this.each(function() {
			var $this = $(this);
			var current = options.current;
			var next = Number(current + 1);
			var prev = Number(current - 1);
			var steps = $('fieldset', this).length;

			$(options.nextBtn).off('click');
			$(options.backBtn).off('click');
			$(options.rstBtn).off('click');
			
			console.log('Current Step', current);

			$($this).data('step', current);
			$('#step' + current).css('display', 'table');

			$(options.nextBtn).one({
				click: function(e){
					e.preventDefault();
					var current = $($this).data('step');
					var validator = $('#step' + current).validate();

					console.log('OnClick Current', ('#step' + current));
					console.log('Validator', validator);
					
					if (validator.success == true) {
						var data = validator.data;
						console.log('Extend Session', {
							'current': $($this).data(),
							'new': data
						});

						$.extend($($this).data(), data);

						$('#step' + current).fadeOut().hide();

						setTimeout(function(){
							var current = $($this).data('step');
							var next = Number(current + 1);
							console.log('The next step', next);
							// if (next <= steps && next != current) {
								$('#step' + next).fadeIn().css('display', 'table');
							// }

							current = ++current;
							next = ++next;
							
							console.log('After change', {
								'current': current,
								'next': next,
								'steps': steps
							});

							$.extend($($this).data(), {step: current});
						}, 500);
					}

					if ($('#promotion').data('step') == 7) {
		                $('span', '#review-table tbody td').each(function(k, v){
		                    var id = $(v).attr('id');
		                    var data = $($this).data(id);

		                    console.log('DATA', data);
		                    
		                    $(this).html(data);
		                });
		            } else if ($('#promotion').data('step') == 9) { // current step == 9
		                $('.selected', '#buzz-table').each(function(){
		                    var input = $('<input type="hidden"/>').attr('name', $(this).attr('name')).val(true);

		                    $('#buzz-table').append(input);
		                });

		                var object = {};
		                $('input[type=hidden]', '#buzz-table').each(function(){
		                	var name = $(this).attr('name');
		                	var val = $(this).val();

		                	object[name] = val;
		                });

		                $($this).data('buzz', object);
		            } else if ($('#promotion').data('step') == 10) { // current step == 10
		                // $('span', '#step 10 #next-btn').html('finish').on('click', function(e){
		                //     e.preventDefault();
		                //     var style = params.style;
		                //     var obj = {};
		                //     $.extend(obj, {'style': style, step: 'init'});
		                //     console.log('Restart Obj', obj);

		                //     $('#reload').trigger('navNext', {step: 1, style: style});
		                // });
		            }

					$(document).trigger('formChange', $($this).data());
				}
			});

			$(options.backBtn).one({
				click: function(e){
					e.preventDefault();
					var current = $($this).data('step');
					var prev = Number(current - 1);

					console.log('OnClick Current', ('#step' + current));
					console.log('PREV', prev);

					$('#step' + current).fadeOut().hide();

					setTimeout(function(){
						var current = $($this).data('step');
						var prev = Number(current - 1);

						if (current > 1) {
							$('#step' + prev).fadeIn().css('display', 'table');
						}

						console.log('prev step', prev);
						
						$.extend($($this).data(), {'step': prev});
					}, 400);

					$(document).trigger('formChange', $($this).data());
				}
			});

			$(options.rstBtn).one({
				click: function(e){
					e.preventDefault();
					var current = $($this).data('step');

					$('#step' + current).hide('fadeOut');

					setTimeout(function(){
						$('#step1').css('display', 'table');

						current = 1;
						$($this).removeData();
						$($this).data('step', current);
					}, 400);

					$(document).trigger('formChange', $($this).data());
				}
			});

			if ($.isFunction(callback)) {
				callback.call(this);
			}
		});
	}

	$.fn.mvForm.defaults = {
		current : 1,
		nextBtn	: $('#NxtBtn'),
		backBtn	: $('#BckBtn'),
		rstBtn	: $('#RstBtn'),
		action	: 'tools/formProcessing.php',
		complete: null,
	};
})(jQuery);

(function($){
	$.fn.validate = function(calllback) {
		// return this.each(function() {
		var elements = $('.answer input, .answer .selected', this);
		var data = {};

		elements.each(function(){
			// Do Validation
			
			// Gather values & place them in data object
			var name = $(this).attr('name');
			var id = $(this).attr('id');
			var value = $(this).val();

			data[name] = value;
		});

		return {
			success: true,
			data: data
		};
		// });
	}
})(jQuery);