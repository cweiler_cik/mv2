(function($){
    var methods = {
        init: function(options, callback) {
            if ($.isFunction(options)) {
                callback = options;
                options = null;
            }

            options = $.extend($.fn.msgBox.defaults, options);

            return this.each(function(){
                var msg = options.msg;
                var msgHead = '<h4>' + options.msgHead + '</h4>';
                var msgId = 'alert-' + options.seed++;
                var type = options.type;
                var block = options.block;
                var alert = $('<div class="alert" />');
                var button = $('<button type="button" class="close" data-dismiss="alert" />');
                
                alert.attr('id', msgId);

                var msgSel = $(alert);

                button.html('&times;');

                if (block == true) {
                    $(msgSel).addClass('alert-block');
                }

                if (type == 'error') {
                    $(msgSel).addClass('alert-error');
                }

                var html = msgHead + msg;

                // // console.log.log('msgSel', $(msgSel));
                // // console.log.log('this', $(this));

                $(msgSel).html(html);
                $(msgSel).prepend(button).fadeOut(10);
                $(this).prepend($(msgSel));

                if ($.isFunction(callback)){
                    callback.call(this);
                }
            });
        }
    }

    $.fn.msgBox = function(method)
    {
        if (methods[method]) {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || !method) {
            return methods.init.apply(this, arguments);
        } else {
            $.error('Method ' +  method + ' does not exist on jQuery.msgBox');
        }
    }

    $.fn.msgBox.defaults = {
        type: 'alert',
        seed: 100000,
        msg: "Best check yo self, you're not looking too good. Nulla vitae elit libero, a pharetra augue. Praesent commodo cursus magna, vel scelerisque nisl consectetur et.",
        msgHead: 'Warning!',
        block: true
    };
})(jQuery);