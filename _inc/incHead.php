<!-- <script type="text/javascript" src="tools/firebug-lite/build/firebug-lite.js"></script> -->
<meta charset="utf-8">
<title>Kiosk Alternatives Prototype</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0;">
<meta name="description" content="">
<meta name="author" content="CIK Enterprises">

<meta name="application-name" content="Marketvision 7 - Style <?php echo $params['style'] ?>"/> 
<meta name="msapplication-TileColor" content="#313131"/> 
<meta name="msapplication-TileImage" content="mv_tile.png"/>

<!-- Le fonts -->
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700' rel='stylesheet' type='text/css'>

<!-- Le styles -->
<link href="css/bootstrap.css" rel="stylesheet">
<link href="css/bootstrap-responsive.css" rel="stylesheet">
<link href="css/kiosks.css" rel="stylesheet">
<link href="css/jquery.selectBoxIt.css" rel="stylesheet">

<link rel="stylesheet" href="../_tools/globals/_css/general.css">
<link rel="stylesheet" href="spin2win/_css/vertical_kiosk.css">
<?php include("spin2win/_inc/settings.php"); ?>

<?php
if ( $style ):
    echo '<link href="css/style' . $style . '.css" rel="stylesheet">';
else:
    echo '<link href="css/style1.css" rel="stylesheet">';
endif;
?>

<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
  <script src="js/html5shiv.js"></script>
<![endif]-->

<!-- Fav and touch icons -->
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="ico/apple-touch-icon-144-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="ico/apple-touch-icon-114-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="ico/apple-touch-icon-72-precomposed.png">
<link rel="apple-touch-icon-precomposed" href="ico/apple-touch-icon-57-precomposed.png">
<link rel="shortcut icon" href="ico/favicon.png">