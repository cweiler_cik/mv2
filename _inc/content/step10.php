<!-- <div id="content" class="content"> -->
    <div class="content-div question">
        <h1 class="q-head">SUCCESS!</h1>
        <p class="q-body">
            Your information has been saved successfully, please click "Finish" to complete your entry!
        </p>
    </div>

    <div class="content-div answer">
        <button class="save-btn reset-btn" id="save-btn" type="submit">
            <span>Finish</span>
        </button>
        <!-- <fieldset id="delivery" name="delivery">
            <div class="inline-input">
                <div class="icon phone-icon"></div>
                <input class="full phone-input" type="tel" name="del-phone" id="del-phone" placeholder="Let me know via SMS (Enter your number)">
            </div>
            <div class="inline-input">
                <div class="icon email-icon"></div>
                <input class="full phone-input" type="email" name="del-email" id="del-email" placeholder="Send me an email (Enter email address)">
            </div>
            <div class="clearfix"></div>
        </fieldset> -->
    </div>
<!-- </div> -->