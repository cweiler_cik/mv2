<!-- <div id="content" class="content"> -->
    <div class="content-div question">
        <h1 class="q-head">Is This You?</h1>
        <p class="q-body">
            Is the displayed info still correct?  We want to make sure everything is accurate!
        </p>
    </div>

    <div class="content-div answer">
        <fieldset id="address-correct" name="address-correct">
            <address>
                <b><span id="customer-fname"></span> <span id="customer-lname"></span></b><br />
                <span id="customer-address"></span><br />
                <span id="customer-city"></span>, <span id="customer-state"></span> <span id="customer-zip"></span>
            </address>
            <div class="kiosk-hr"></div>
            <div class="buttons-row">
                <span>
                    Is everything correct?
                </span>
                <div class="buttons">
                    <div class="icon-btn checkmark"></div>
                    <div class="icon-btn x-icon"></div>
                </div>
            </div>
        </fieldset>
    </div>

    <div class="content-div hidden-content">
        <fieldset id="edit-address" name="edit-address">
            <div class="input-wrap input-row">
                <input class="half validator unvalidated" type="text" placeholder="First Name" name="edit-customer-fname" id="edit-customer-fname">
                <input class="half validator unvalidated" type="text" placeholder="Last Name" name="edit-customer-lname" id="edit-customer-lname">
            </div>
            <div class="input-wrap input-row">
                <input class="full validator unvalidated" type="text" placeholder="Address" name="edit-customer-address" id="edit-customer-address">
            </div>
            <div class="input-wrap input-row">
                <input class="third validator unvalidated" type="text" placeholder="City" name="city" id="edit-customer-city">
                <input class="third validator unvalidated" type="text" placeholder="State" name="state" id="edit-customer-state">
                <input class="third validator unvalidated" type="text" placeholder="Zip" name="zip" id="edit-customer-zip" pattern="[0-9]*" maxlength="5">
            </div>

            <div class="clearfix"></div>
            <div class="kiosk-hr"></div>
            
            <div class="buttons-row">
                <span>
                    Everything looking good?
                </span>
                <div class="buttons">
                    <div class="icon-btn checkmark"></div>
                    <div class="icon-btn x-icon"></div>
                </div>
            </div>
        </fieldset>
    </div>
<!-- </div> -->