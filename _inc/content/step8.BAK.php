<!-- <div id="content" class="content"> -->
    <div class="content-div question">
        <h1 class="q-head">Just the Facts</h1>
        <p class="q-body">
            Based on the information you gave us about your car, our <b>unbeatable NADA computation machine</b> gave us a great trade value!
        </p>
    </div>

    <div class="content-div answer">
        <table id="review-table" cols="4">
            <tbody>
                <tr>
                    <th>Name:</th>
                    <td colspan="3">
                        <span id="user_fname"></span>&nbsp;<span id="user_lname"></span>
                        <?php // echo $params['user_fname'] . ' '. $params['user_lname'] ?>
                    </td>
                </tr>
                <tr>
                    <th>Address:</th>
                    <td colspan="3">
                        <address>
                            <span id="address"></span><br/>
                            <span id="city"></span>,&nbsp;<span id="state"></span>&nbsp;<span id="zip"></span>
                            <?php
                            // echo $params['address'] . '<br />';
                            // echo $params['city']. ', ' . $params['state'] . ' ' . $params['zip'];
                            ?>
                        </address>
                    </td>
                </tr>
                <tr>
                    <th>Vehicle:</th>
                    <td colspan="3">
                        <span id="year"></span>&nbsp;<span id="make"></span> with <span id="mileage"></span>
                        <?php
                        // echo $params['year'] . ' ' . $params['make'] . ' with ' . $params['mileage'] . ' miles';
                        ?>
                    </td>
                </tr>
                <tr>
                    <th>Authorized Dealer:</th>
                    <td colspan="3">
                        <span id="dealer_name"></span>
                        <?php // echo $params['dealer_name'] ?>
                    </td>
                </tr>
            </tbody>
            <tfoot>
                <tr>
                    <td colspan="4">
                        <div id="trade-value">
                            <div class="values-combined">
                                <div id="nada">
                                    <span>$15,950.00</span><br />
                                    NADA Blue Book Value
                                </div>
                                <span class="green-plus">+</span>
                                <div id="trade-bonus">
                                    <span>$3,190.00 *</span><br />
                                    120% Trade Bonus
                                </div>
                            </div>
                            
                            <div class="clearfix"></div>
                            <div class="kiosk-hr"></div>
                            
                            <div class="final-value">
                                <img src="img/green-circle.gif" alt="Circled" title="Green Circle"/>
                                <div class="circled">= $19,140.00 **</div><br />
                                Trade Value
                            </div>
                        </div>
                    </td>
                </tr>
            </tfoot>
        </table>
        <div class="disclaimer">
            <span class="green">* Offer expires 09/29/2013</span><br />
            ** Don't forget you still need an appraisal!
        </div>
    </div>
<!-- </div> -->