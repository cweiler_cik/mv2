<!-- <div id="content" class="content"> -->
    <div class="content-div question">
        <h1 class="q-head">Thank You!</h1>
        <p class="q-body">
            Choose how you would like to receive your prize confirmation from the options to the right, and we look forward to seeing you again!
        </p>
    </div>

    <div class="content-div answer">
        <div class="inline-input">
            <div class="icon email-icon"></div>
            <input class="full email-input validator unvalidated" type="email" name="contact-email" id="contact-email" placeholder="Send me an email">
        </div>
        <!-- <div class="reset-btn" id="restart-btn">
            <span>Restart</span>
        </div> -->
        <!-- <fieldset id="delivery" name="delivery">
            <div class="inline-input">
                <div class="icon phone-icon"></div>
                <input class="full phone-input" type="tel" name="del-phone" id="del-phone" placeholder="Let me know via SMS (Enter your number)">
            </div>
            <div class="inline-input">
                <div class="icon email-icon"></div>
                <input class="full phone-input" type="email" name="del-email" id="del-email" placeholder="Send me an email (Enter email address)">
            </div>
            <div class="clearfix"></div>
        </fieldset> -->
    </div>
<!-- </div> -->