<!-- <div id="content" class="content"> -->
    <div class="content-div question">
        <h1 class="q-head">Ch-Ch-Changes!</h1>
        <p class="q-body">
            Listen, we know you love your kitchen appliances but what if...
        </p>
    </div>
    
     <div class="content-div answer">
        <fieldset id="step5" name="step5">
            <p class="question">
                Q: If you could purchase new kitchen appliances at an affordable price, when would you decide to do so?
            </p>
            <div>
                <div class="input-row input-wrap">
                    <select class="kiosk-select unvalidated" name="when" id="when">
                        <option value="">When are you looking?</option>
                        <option value="today">Now</option>
                        <option value="month">This month</option>
                        <option value="3months">In the next 3 months</option>
                        <option value="6months">In the next 6 months</option>
                        <option value="year">In the next year</option>
                    </select>
                </div>
            </div>

            <div id="when-wrap" style="display:none;">
                <div class="kiosk-hr"></div>
               
               <div class="input-row input-wrap choose-one">
               		<div class="input-row input-wrap choose-one">
                    	<div class="icon"><span>new</span></div>
                    	<div class="icon"><span>used</span></div>
                   		<input class="validator" type="hidden" name="condition" id="condition" value="">
                   </div>
                </div>
                
            </div>

            <div class="clearfix"></div>
        </fieldset>
    </div>
<!-- </div> -->