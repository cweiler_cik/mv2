<?php
$params = $_POST;
?>

<div class="container" style="display:none;">

    <div id="content" class="content">
        <div class="content-inner">
            <div class="question pull-left">
                <h1><span style="font-weight:normal;">You're</span> Done!</h1>
                <p class="lead">
                    Well <?php echo $params['first_name'] ?>, now all you have to do is click the button to the right to start over!
                </p>
            </div>

            <div class="answer pull-right">
                <div style="height:220px; width:90%; margin:0 auto; position:relative">
                    <button type="button" class="btn btn-primary btn-large" id="restart" style="bottom:50%; position:absolute;">
                        Start Over
                    </button>
                </div>
            </div>
        </div>
    </div>

</div> <!-- /container -->

<div id="BckBtn">
    <a href="content/step8.php" id="back-btn" rel="back">
        <img alt="Back" title="Back" src="img/arrow_Back.png" />
        <span>back</span>
    </a>
</div>

<!-- <div id="NxtBtn">
    <a href="content/step3.php" id="next-btn" rel="next">
        <span>next</span>
        <img alt="Next" title="Next" src="img/arrow_Next.png" />
    </a>
</div> -->

<script>
$(function() {
    var params = '<?php echo json_encode($params) ?>';
    params = $.parseJSON(params);

    initNavigation(params);

    $('#restart').on({
        click: function(e){
            e.preventDefault();
            window.location.href = "index.php";
        }
    });
});
</script>