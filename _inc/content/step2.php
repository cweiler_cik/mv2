<!-- <div id="content" class="content"> -->
    <div class="content-div question">
        <h1 class="q-head">Salesperson Entry</h1>
        <p class="q-body">
            Hopefully you are currently with one of the many <em>awesome</em> people we have on staff.  Have them select their name so we know who helped you!
        </p>
    </div>

    <div class="content-div answer">
        <fieldset id="sp-select" name="sp-select">
            <select name="salesperson" id="salesperson" class="kiosk-select unvalidated required">
                <option value=""> - Select - </option>
                <?php
                foreach ( $params['sp'] as $val ) {
                    echo "<option value=\"{$val['name']}\" data-sp-id=\"{$val['id']}\">{$val['name']}</option>";
                }
                ?>
            </select>
            <div class="kiosk-hr"></div>
            <div class="buttons-row add-option">
                <span>
                    Don't see your name? Click <b>'+'</b> to add.
                </span>
                <div class="buttons">
                    <div class="icon-btn add-one"></div>
                </div>
            </div>
        </fieldset>
    </div>

    <div class="content-div hidden-content">
        <div id="sp-entry-form">
            <div class="input-row input-wrap">
                <input class="full validator unvalidated" type="text" placeholder="First name" id="sp-fname" name="sp-fname">
            </div>
            <div class="input-row input-wrap">
                <input class="full validator unvalidated" type="text" placeholder="Last name" id="sp-lname" name="sp-lname">
            </div>
            <div class="input-row input-wrap">
                <input class="full validator unvalidated" type="email" placeholder="Email address" id="sp-email" name="sp-email">
            </div>
            <div class="clearfix"></div>
            <div class="kiosk-hr"></div>
            <div class="buttons-row">
                <span>
                    All finished?
                </span>
                <div class="buttons">
                    <div class="icon-btn checkmark"></div>
                    <div class="icon-btn x-icon"></div>
                </div>
            </div>
        </div>
    </div>
<!-- </div> -->