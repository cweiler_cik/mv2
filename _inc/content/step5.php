<!-- <div id="content" class="content"> -->
    <div class="content-div question">
        <h1 class="q-head">What Are You Driving?</h1>
        <p class="q-body">
            We love cars - as you <em>might</em> have guessed.  Tell us what you are currently driving!
        </p>
    </div>

    <div class="content-div answer">
        <fieldset id="trade-value" name="trade-value">
            <div class="input-wrap input-row">
                <select class="full kiosk-select unvalidated" id="year" name="year">
                    <option value="">YEAR</option>
                    <?php
                    for ($i = 2013; $i >= 1985; $i--) {
                        echo "<option value=\"{$i}\">{$i}</option>";
                    }
                    ?>
                </select>
            </div>
            <div class="input-wrap input-row">
                <select class="full right kiosk-select unvalidated" id="make" name="make">
                    <option value="">MAKE</option>                  
                </select>
            </div>
            <div class="input-wrap input-row">
                <select class="full kiosk-select unvalidated" id="model" name="model">
                    <option value="">MODEL</option>
                </select>
            </div>
            <div class="input-wrap input-row">
                <select class="full kiosk-select unvalidated" id="mileage" name="mileage">
                    <option value="">MILEAGE</option>
                    <option value="10000">Below 10,000</option>
                    <option value="30000">10,000 - 30,000</option>
                    <option value="50000">30,000 - 50,000</option>
                    <option value="70000">70,000 - 100,000</option>
                    <option value="100000">100,000 - 120,000</option>
                    <option value="120000">Over 120,000</option>
                </select>
            </div>
            <div class="clearfix"></div>
            <div class="skip-btn">
                <span>
                    <b>Don't Have</b> A Car
                </span>
            </div>
        </fieldset>
    </div>
<!-- </div> -->