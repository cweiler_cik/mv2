<!-- <div id="content" class="content"> -->
    <div id="errorMsg"></div>
    <div class="content-div question">
        <h1 class="q-head">Let's Get Started</h1>
        <p class="q-body">
            Please enter the <b>11-digit PIN</b> from your mail piece by tapping the box to the right!
        </p>
    </div>

    <div class="content-div answer">
        <fieldset id="user-pin" name="user-pin">
            <div class="input-wrap">
                <input class="validator unvalidated" type="number" placeholder="Tap here to begin" id="pin" name="pin" value="" />
                <!-- <span class="bubble help-block"><em>Don't have one on-hand?  Just click below!</em></span> -->
            </div>
            <div class="clearfix"></div>
            <div class="no-pin-btn">
                <span>
                    <b>Don't Have</b> A PIN
                </span>
            </div>
        </fieldset>
    </div>
<!-- </div> -->