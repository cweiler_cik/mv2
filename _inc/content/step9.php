<!-- <div id="content" class="content"> -->
    <div class="content-div question">
        <h1 class="q-head">All the Buzz!</h1>
        <p class="q-body">
            We try to let people know how great we are in many different ways.  How did you hear about us?
        </p>
    </div>

    <div class="content-div answer">
        <fieldset id="step9" name="step9">
            <p class="question">
                Select all that apply:
            </p>
            <table id="buzz-table">
                <tr>
                    <td>
                        <div name="directmail" class="icon-btn directmail-icon"></div>
                    </td>
                    <td>
                        <div name="tv" class="icon-btn tv-icon"></div>
                    </td>
                    <td>
                        <div name="news" class="icon-btn news-icon"></div>
                    </td>
                </tr>

                <tr>
                    <td>
                        <div name="radio" class="icon-btn radio-icon"></div>
                    </td>
                    <td>
                        <div name="billboard" class="icon-btn billboard-icon"></div>
                    </td>
                    <td>
                        <div name="internet" class="icon-btn internet-icon"></div>
                    </td>
                </tr>
            </table>
        </fieldset>
    </div>
<!-- </div> -->