<!-- <div id="content" class="content"> -->
    <div class="content-div question">
        <h1 class="q-head">Who Does Your Maintenance?</h1>
        <p class="q-body">
            Q: Who do you call when you have repair needs on your appliances?
        </p>
    </div>

    <div class="content-div answer">
        <fieldset id="service" name="service">
            <div class="input-wrap">
                <select class="kiosk-select unvalidated" name="mechanic-select" id="mechanic-select">
                    <option value="">Select the most appropriate answer:</option>
                    <option value="This dealership">This dealership</option>
                    <option value="Another dealership">Another dealership</option>
                    <option value="Express lube/discount store">Express lube/discount store</option>
                    <option value="Do it yourself">Do it yourself</option>
                </select>
                <!-- <input class="validator" type="hidden" name="mechanic" id="mechanic" value=""> -->
            </div>
            <div class="clearfix"></div>
        </fieldset>
    </div>
<!-- </div> -->