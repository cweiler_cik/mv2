<!-- <div id="content" class="content"> -->
    <div class="content-div question">
        <h1 class="q-head">If You Win...</h1>
        <p class="q-body">
            How can we let you know?  We can also let you know about service specials and some pretty cool discounts.
        </p>
    </div>

    <div class="content-div answer">
        <fieldset id="notify-by" name="notify-by">
            <div class="inline-input">
                <div class="icon phone-icon"></div>
                <input class="full phone-input validator unvalidated" type="tel" name="phone" id="phone" placeholder="Call me (Enter your number)" onclick="this.setSelectionRange(0,0);">
            </div>
            <div class="inline-input">
                <div class="icon email-icon"></div>
                <input class="full email-input validator unvalidated" type="email" name="email" id="email" placeholder="Send me an email">
            </div>
            <div class="clearfix"></div>
        </fieldset>
    </div>
<!-- </div> -->