<?php

session_start();
include("../../db.php");
include("../../fn.php");
//fnEstablishGeneralConnection();

switch ($_REQUEST['action']):

// !CHECK PIN ENTERED BY CUSTOMER //////////////////////////////////////////////////////////////////////////////////////////////////
	case 'check_pin':
		$dbh = new cikPdo;
		$dbh = $dbh->dbh;
		$params = $_REQUEST;
		
		if ( $params['user_agent'] != 'ie' ):
			header( 'Content-type: application/json' );
		else:
			header( 'Content-type: text/plain' );
		endif;
		
		$key = $params['key'];
		$job = $params['job'];
		
		if ($params['subjob']):
			$subjob = $params['subjob'];
		else:
			$subjob = 1;
		endif;

		if ( $key && $key != '' ):
			try {
		
				$q = "SELECT * FROM `tcustomer` WHERE CustomerKey = :key AND jobs_no = :job";
								
				$stmt = $dbh->prepare( $q );
				$stmt->bindParam( ':key', $key );
				$stmt->bindParam( ':job', $job );
				$stmt->execute();
				
			} catch ( PDOException $e ) {
				exit( $e->getMessage() );
			}
		else:
			$data['result'] = 'Error: No Key Submitted';
		endif;
		
		$result = $stmt->fetchAll( PDO::FETCH_ASSOC );
		
		if ( $result ):
			foreach ( $result as $row ) {
				$data['hasResults'] = true;
				$data['result'] = $row;
			}
		else:
			if ($key == 81080500000):
				$data['hasResults'] = true;
				$data['result'] = array('key' => 'value');
			else:
				$data['hasResults'] = false;
				$data['result'] = null;
			endif;
		endif;
		
		//$json = json_encode( $data );
		exit( json_encode( $data ) );

	break;
	
// !CREATE NEW CUSTOMER //////////////////////////////////////////////////////////////////////////////////////////////////
	case 'create_customer':
		$dbh 	= new cikPdo;
		$dbh	= $dbh->dbh;
		$params	= $_REQUEST;
		
		$result = createCustomer( $params );
		// $json 	= json_encode( $result );
		$json 	= $result;
		
		if ( $params['user_agent'] != 'ie' ):
			header( 'Content-type: application/json' );
		else:
			header( 'Content-type: text/plain' );
		endif;
		
		exit( $json );

	break;

// !UPDATE EXSISTING CUSTOMER //////////////////////////////////////////////////////////////////////////////////////////////////
	case 'update_customer':
		$dbh 	= new cikPdo;
		$dbh 	= $dbh->dbh;
		$params	= $_REQUEST;
		
		$job				= $params['job'];
		$drops_no			= $params['drop'];
		$scanned 			= 'T';
		$key				= $params['customer-key'];
		$fname 				= $params['customer-fname'];
		$lname 				= $params['customer-lname'];
		$address 			= $params['cutsomer-address'];
		$city 				= $params['customer-city'];
		$state 				= $params['customer-state'];
		$zip 				= $params['customer-zip'];
		$email 				= $params['customer-email'];
		$year 				= $params['year'];
		$make 				= $params['make'];
		$model 				= $params['model'];
		$mileage			= $params['mileage'];
		$yearMakeModel 		= $year . ' ' . $make . ' ' . $model;
		$phone 				= $params['phone'];
		$cell 				= $params['cell'];
		// $scanDate 		= $params['scanDate'];
		$scanDate 			= date( 'm-d-Y', time() );
		$start 				= $params['startTime'];
		$finish 			= mktime( date("H"), date("i"), date("s"), date("m"), date("d"), date("Y") );
		$spId 				= $params['sp'];
		$test 				= 'F';
		$followUp 			= 'F';
		
		$params['scanDate'] = $scanDate;
		
		if ($params['subjob']):
			$subjob = $params['subjob'];
		else:
			$subjob = 1;
		endif;
		
		$q = "UPDATE `tcustomer` SET
				`Scanned` 					= :scanned,
				`CustomerFirstNameChange` 	= :fname,
				`CustomerLastNameChange` 	= :lname,
				`CustomerAddressChange`		= :address,
				`CustomerCityChange` 		= :city,
				`CustomerStateChange`		= :state,
				`CustomerZIPChange` 		= :zip,
				`CustomerYearMakeModel` 	= :yearMakeModel,
				`CustomerEmail` 			= :email,
				`CustomerPhone` 			= :phone,
				`CustomerCell` 				= :cell,
				`ScanDate` 					= :scanDate,
				`StartTime` 				= :start,
				`EndTime` 					= :finish,
				`SalespersonID` 			= :spId,
				`TestRecord` 				= :test,
				`FollowUp` 					= :followUp
			WHERE `CustomerKey` = :key
			AND `jobs_no` 		= :job";
		
		try {
			$stmt = $dbh->prepare( $q );
		
			$stmt->bindParam( ':key', $key );
			$stmt->bindParam( ':job', $job );
			$stmt->bindParam( ':scanned', $scanned );
			$stmt->bindParam( ':fname', $fname );
			$stmt->bindParam( ':lname', $lname );
			$stmt->bindParam( ':address', $address );
			$stmt->bindParam( ':city', $city );
			$stmt->bindParam( ':state', $state );
			$stmt->bindParam( ':zip', $zip );
			$stmt->bindParam( ':yearMakeModel', $yearMakeModel );
			$stmt->bindParam( ':email', $email );
			$stmt->bindParam( ':phone', $phone );
			$stmt->bindParam( ':cell', $cell );
			$stmt->bindParam( ':scanDate', $scanDate );
			$stmt->bindParam( ':start', $start );
			$stmt->bindParam( ':finish', $finish );
			$stmt->bindParam( ':spId', $spId );
			$stmt->bindParam( ':test', $test );
			$stmt->bindParam( ':followUp', $followUp );	
			
			$result = $stmt->execute();
		} catch ( PDOException $e ) {
			// return json_encode($e->getMessage());
			exit(json_encode($e->getMessage()));
		}
		
		if ( $params['user_agent'] != 'ie' ):
			header( 'Content-type: application/json' );
		else:
			header( 'Content-type: text/plain' );
		endif;
		
		if ( $result && $stmt->rowCount() > 0 ) {
			// return true;
			// return json_encode(array( 'success' => true, 'message' => $stmt->rowCount() . ' rows affected' ));
			exit( json_encode(array( 'success' => true, 'message' => $stmt->rowCount() . ' rows affected', 'result' => $result )) );
		} else {
			// return false;
			exit( json_encode(array( 'success' => false, 'message' => 'No rows affected' )) );
		}
		// var_dump( $json );
		// exit;
		exit( $json );
		
	break;
	
// !UPDATE PRIZES  //////////////////////////////////////////////////////////////////////////////////////////////////
	case 'update_prizes':
		$dbh 	= new cikPdo;
		$dbh 	= $dbh->dbh;
		$params	= $_REQUEST;
		
		$result = updatePrizes( $params );
		$json 	= json_encode( $result );
		
		if ( $params['user_agent'] != 'ie' ):
			header( 'Content-type: application/json' );
		else:
			header( 'Content-type: text/plain' );
		endif;
		
		exit( $json );
	break;
	
// !UPDATE PRIZE LOG  //////////////////////////////////////////////////////////////////////////////////////////////////
	case 'update_prize_log':
		$dbh 	= new cikPdo;
		$dbh 	= $dbh->dbh;
		$params	= $_REQUEST;
		
		$result = updatePrizeLog( $params );
		$json 	= json_encode( $result );
		
		if ( $params['user_agent'] != 'ie' ):
			header( 'Content-type: application/json' );
		else:
			header( 'Content-type: text/plain' );
		endif;
		
		exit( $json );
	break;
		
endswitch;

//fnCloseGeneralConnection();
?>