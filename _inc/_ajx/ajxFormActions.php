<?php

session_start();
include("../../db.php");
include("../../fn.php");
//fnEstablishGeneralConnection();

switch ($_REQUEST['action']):

// !PROCESS POST //////////////////////////////////////////////////////////////////////////////////////////////////
	case 'process_post':
		$params = $_REQUEST;
		//print_r($params); exit;
		
		if ( $params['needs-created'] == 'true' ) {
			$operation = createCustomer( $params );
		} else {
			$operation = updateCustomer( $params );
		}
		
		if ( $params['isWinner'] == 'true' ) {
			$updates = updatePrizes( $params );
		} else {
			$updates = updatePrizeLog( $params );
		}
		
		$result = array(
			'operation' => $operation,
			'updates'	=> $updates,
			'debug'		=> $params
			);
		
		exit( json_encode( $result ) );
	
	break;
	
endswitch;

//fnCloseGeneralConnection();
?>