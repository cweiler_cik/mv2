<?

// USE FOR TRADITIONAL SITE PAGES
	// !INCLUDE PAGE BASED ON URL PAGE ID
	/*
	switch($vPagePath) {
		case 'Account': 		include("_inc/incUserAccount.php");		break;
		case 'Dashboard':		include("_inc/incDashboard.php");  		break;
		case 'History':			include("_inc/incQuoteHistory.php");	break;
		case 'Library':			include("_inc/incDocs.php");			break;
		case 'Logout':			include("_inc/incLogout.php");			break;
		case 'Opportunities':	include("_inc/incOpportunities.php");	break;
		case 'Quote':			include("_inc/incQuoteSummary.php");	break;
		case 'Repairs':			include("_inc/incRepairs.php");			break;
		case 'Search':			include("_inc/incSearchRS.php");		break;
		case 'SignIn':			include("_inc/incSignIn.php");			break;
	    case 'Updates':			include("_inc/incUpdates.php");    		break;
		default:				include("_inc/incPageNotFound.php");	break;
	}
*/


// USE FOR KIOSK PANELS
?>
    <div id="reload-wrap" class="container">
        <div id="error"></div>

        <!-- <form id="promotion" name="promotion" action="lib/formProcessor.php" method="post"> -->
        <form id="promotion" name="promotion" action="_inc/_ajx/ajxFormActions.php?action=process_post" method="post">
            <div id="reload">            
                <!-- <div id="reload"></div> -->
                <div class="content" id="step1">
                    <?php include( '_inc/content/step1.php' ); ?>
                </div>
                <div class="content" id="step2">
                    <?php include( '_inc/content/step2.php' ); ?>
                </div>
                <div class="content" id="step3">
                    <?php include( '_inc/content/step3.php' ); ?>
                </div>
                <div class="content" id="step4">
                    <?php include( '_inc/content/step4.php' ); ?>
                </div>
				<div class="content" id="step5">
					<?php include( '_inc/content/step7.php' ); ?>
                    <?php //include( '_inc/content/step5.php' ); ?>
                </div>
                <div class="content" id="step6">
					<?php include('spin2win/index.php'); ?>
                    <?php //include( '_inc/content/step6.php' ); ?>
                </div>
                <div class="content" id="step7">
                    <?php include( '_inc/content/step8.php' ); ?>
                    <?php //include( '_inc/content/step7.php' ); ?>
                </div>
                
				<div class="content" id="step8">
                    <?php include( '_inc/content/step10.php' ); ?>
                    <?php //include('spin2win/index.php'); ?>
                </div>
                <!--
                <div class="content" id="step9">
                    <?php //include( '_inc/content/step10.php' ); ?>
                    <?php //include( '_inc/content/step8.php' ); ?>
                    <?php //include( '_inc/content/step9.php' ); ?>    
                </div>
                
                <div class="content" id="step10">
                    <?php //include( '_inc/content/step10.php' ); ?>
                </div>
				-->    
            </div>
        </form>
    </div>

    <div id="BckBtn">
        <a href="_inc/content/step1.php" id="back-btn" rel="back">
            <img alt="Back" title="Back" src="img/style<?php echo $style ?>/back-icon.png" />
            <span>back</span>
        </a>
    </div>

    <div id="NxtBtn">
        <a href="_inc/content/step2.php" id="next-btn" rel="next">
            <span>next</span>
            <img alt="Next" title="Next" src="img/style<?php echo $style ?>/next-icon.png" />
        </a>
    </div>

    <div id="RstBtn">
        <a id="restart-btn" href="_inc/content/step1.php" rel="restart">
            <span>restart</span>
            <img alt="Restart" title="Restart" src="img/style<?php echo $style ?>/refresh-icon.png"/>
        </a>
    </div>