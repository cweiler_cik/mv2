<script type="text/javascript">
	/* JAVASCRIPT VARS FROM PHP VARS */
	
	// INTS
		var onsurface 		= 0;
		var style_id 		= <?php echo $style; ?>;
		
		// MAKE THE WINNING SPIN COUNT RANDOMIZED
		//var winner_count = randomFrom(spin_count, 1);
		//console.log('winner_count: ' + winner_count);
		
		// MAKE THE WINNING SPIN COUNT STATIC
		var winner_count   	= <?php echo $vWinnerCount; ?>;
	    var init_spins 	 	= <?php echo $vInitialSpins; ?>;
	    var extra_spins  	= <?php echo $vExtraSpins; ?>;
	    var prize_cnt		= <?php echo $vPrizeCnt; ?>;
	    //var spin_landing 	= (<?php //echo $vPrizeCnt; ?> * 8) + <?php //echo $vWinnerPrize; ?>
	
	// ARRAYS
	    var shortText 	 	= <?php echo json_encode($aH2Feedback); ?>;
	    var longText  	 	= <?php echo json_encode($aH5Feedback); ?>;
	    
    // STRINGS
	    var continue_url 	= "<?php echo $vNextPage; ?>";
	    var iShortText	 	= "<?php echo $iH2Feedback; ?>";
	    var iLongText	 	= "<?php echo $iH5Feedback; ?>";
	    var wShortText	 	= "<?php echo $wH2Feedback; ?>";
	    var wLongText		= "<?php echo $wH5Feedback; ?>";
	    
		switch (style_id){
			case 1:	var primary_color = '#0274BA', secondary_color = '#EB8A01'; break;	
			case 2:	var primary_color = '#0274BA', secondary_color = '#EB8A01'; break;	
			case 3:	var primary_color = '#0274ba', secondary_color = '#995754';	break;
			case 4:	var primary_color = '#0274ba', secondary_color = '#995754';	break;
			case 5: var primary_color = '#e71938', secondary_color = '#fdb823';	break;
		}
</script>