/* SETUP JAVASCRIPT VARS FOR PRIZE WHEEL //////////////////////////////////////////////////////////////////////////////////////*/
var sounds = new SFX('spin2win/_audio/{{type}}');
sounds.add( 'spin', 'spinloop', 50 );
sounds.add( 'win', 'success', 50 );
sounds.add( 'loser', 'lost', 50 );

var spin_count 	= [];
var spins 		= init_spins;

for (var i=1; i<=(spins+extra_spins); i++) {
	spin_count.push(i);
}

$(window).load(function() {
	
/* BEGIN JAVASCRIPT ACTIONS ////////////////////////////////////////////////////////////////////////////////////////////////////*/
	
	// INITIALLIZE DISPLAY TEXT IN DIVS AND ON BUTTONS
	$(".feedback_big").html(iShortText);
	$(".feedback_small").html(iLongText);
	$(".counter_number").html((spins+extra_spins));
	$(".remaining_spins").html("spins remaining");
	$('#spin_btn').html("spin");

	// INITIALLIZE DISPLAY COLORS FROM DB
	$(".feedback_big").css("color", primary_color);
	if(style_id != 5){ $(".feedback_small").css("color", primary_color); }
	$("#spin_btn").css("background-color", primary_color);
	$(".counter_number").css("color", secondary_color);
	$(".pointer_spacer").css("background-color", secondary_color);
	$(".pointer_right").css("border-left-color", secondary_color);
	
	// SPIN BUTTON ROLLOVER STATE EFFECTS
	$("#spin_btn").mousedown( function() {
		$(this).css("box-shadow", "inset 0 0 8px #EFEFEF");
		$(this).css("-moz-box-shadow", "inset 0 0 8px #EFEFEF");
		$(this).css("-webkit-box-shadow", "inset 0 0 8px #EFEFEF");
	});
	$("#spin_btn").mouseout( function() {
		$(this).css("box-shadow", "none");
		$(this).css("-moz-box-shadow", "none");
		$(this).css("-webkit-box-shadow", "none");
	});
	$("#spin_btn").mouseover( function() {
		$(this).css("box-shadow", "0px 2px 6px #666");
		$(this).css("-moz-box-shadow", "0px 2px 6px #666");
		$(this).css("-webkit-box-shadow", "0px 2px 6px #666");
	});
	$("#spin_btn").mouseup( function() {
		$(this).css("box-shadow", "none");
		$(this).css("-moz-box-shadow", "none");
		$(this).css("-webkit-box-shadow", "none");
	});

/*	
	elm.addEventListener("MSPointerDown",handleDown,false);
	function handleDown(evt) {
		if(evt.pointerType == evt.MSPOINTER_TYPE_TOUCH) {
			// Do something for touch input only
			alert("Spinnnn!!!");
	    }else{
		    // Do something for non-touch input
		}
	}
*/
	
	$('#carousel').carouFredSel({		
		direction	: 'down',
		auto		: false,		
		width		: '100%',
		height      : 'variable',
		items	 : {
			width		: 191,
			height      : 390,
			visible		: 3
		}
/*
		swipe: {
			fx: "scroll",
			onBefore : function( ) {
				sounds.play('spin', true);
			},
			duration : 4000,
            
            items	 : spin_landing,
            
            onAfter  : function( ) {
            	sounds.stop('spin');
            }
		}
*/
	});

	var counter = 0;
	var counter_number = spin_count.length;
	
	// SPIN BUTTON ONCLICK
	$('#spin_btn').click(function() {	
		var spin_landing = parseInt($('#promotion').data('customer-prizeid'));
		counter++;
		console.log(spin_landing);
		// SPIN THE WHEEL
		if($('#promotion').data('game-played') == 1 || counter == (winner_count+1)){
			//document.location.href = continue_url;
			$('#promotion').data('game-played', 1);
			$('#NxtBtn').click();
		}else{
			// CAROUSEL TRIGGER
			$("#carousel").trigger("prev", [35, true, function( ) {
				// STOP SOUND FX WHEN WHEEL STOPS
				sounds.stop("spin");
							
				// ADD CLASS TO WINNING ELEMENT
				$("img").each(function( index ){
			    	var highlight = onsurface ? 2 : 2;
			    	if(index == highlight){
			    		$(this).addClass("win_border");
			    	}
			    });
			    
				if(counter == winner_count){						
					sounds.play("win");
					// MAKE BUTTON CLICKABLE AGAIN
					$("#spin_btn").prop('disabled',false);
					
					// CHANGE TEXT ON BUTTON					
					$("#spin_btn").html("continue");
					
					//CHANGE THE FEEDBACK AREAS
					$(".feedback_big").html(wShortText);
					$(".feedback_small").html(wLongText);
					$(".counter_number").html(counter_number);
					if(counter_number == 1){ $(".remaining_spins").html("spin remaining"); }else{ $(".remaining_spins").html("spins remaining"); }
					
				}else{
					sounds.play("loser");
					// MAKE BUTTON CLICKABLE AGAIN
					$("#spin_btn").prop('disabled',false);
										
					//CHANGE THE FEEDBACK AREAS
					$(".feedback_big").html(randomFrom(shortText, false));
					$(".feedback_small").html(randomFrom(longText, false));
					$(".counter_number").html(counter_number);
					if(counter_number == 1){ $(".remaining_spins").html("spin remaining"); }else{ $(".remaining_spins").html("spins remaining"); }
				}
				
			}, {
				duration : 3700
			}]);
			
			// PLAY SOUND FX
			sounds.play('spin', true);	
			
			// MAKE BUTTON DISABLED
			$(this).prop('disabled',true);
			
			// COUNT DOWN THE NUMBER OF SPINS
			counter_number--;
			return false;
		}
	});

});