<?php
// INTIALLIZE VARS FROM DB

// INTS
switch($style):
	case 1: case 2:	case 3:	case 4: 
		$vPrizeCnt = 8;
		$aPrizes   = array( '10k Cash', '$2 Cash', 'Cars or 100k Cash', '32GB Apple iPad', 'Traeger Smoker', 'TV', 'Wii', '$500 Cash' );
	break;
	case 5: 
		$vPrizeCnt = 4;
		$aPrizes   = array( '$10 HHGregg Gift Card', '$25 HHGregg Gift Card', '$100 HHGregg Gift Card', 'Pick A Prize Table' );
	break;
endswitch;

$vInitialSpins	 = 1;
$vExtraSpins	 = 0;
$vWinnerCount	 = 1;
//$vWinnerPrize	 = 7;

// STRINGS
$vNextPage		 = '';
$vCompanyLogo 	 = 'prizewheel_logo.png';
$iH2Feedback	 = 'SPIN THE WHEEL!';
$iH5Feedback	 = 'Tap the spin button to see what you&#39;ve won!';
$wH2Feedback	 = 'WINNER!';
$wH5Feedback	 = 'Hit &ldquo;continue&rdquo; to find out what you&#39;ve won!';

// ARRAYS
$aH2Feedback	 = array( 'TRY AGAIN', 'GOOD TRY', 'ONCE MORE' );
$aH5Feedback	 = array( 'So close, but not quite!', 'If at first you don&#39;t succeed, spin again!', 'You almost had it, keep trying!' );
?>