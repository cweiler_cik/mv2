<!-- GAME WRAPPER -->
<div id="game_wrapper">

	<!-- DETAILS WRAPPER -->
	<div class="float_left" id="details_wrapper">
			
		<!-- DETAILS LOGO -->
		<div class="float_left align_center" id="details_logo">
			<?php //echo "<img src='spin2win/_img/".$vCompanyLogo."' width='260' />"; ?>
		</div>

		<!-- DETAILS TEXT WRAPPER -->
		<div class="float_left" id="details_text_wrapper">
			<!-- DETAILS TEXT AREA -->
			<div class="float_left" id="details_text">
				<div class="feedback_big"></div>
				<div class="feedback_small"></div>
			</div>
		</div>
		
		<!-- SPIN FEEDBACK -->
		<div class="float_left" id="spin_feedback_wrapper">
			<div class="float_right" id="spin_feedback_background">
				<!-- SPIN BUTTON -->
				<div class="float_right" id="spin_btn_div">
					<button id='spin_btn' type='button'></button>
				</div>
				
				<!-- COUNTER BADGE -->
				<div id="spin_counter_wrapper" class="float_left">
					<div class="counter_number float_left align_center"></div>
					<div class="remaining_spins float_left align_center"></div>
				</div>
			</div>
		</div>
		
	</div>

	<!-- DETAILS POINTER -->
	<div id="point_wrapper" class="float_left">
		<div class="pointer_spacer float_left"></div>
		<div class="pointer_right float_left"></div>
	</div>
	
	<!-- CAROUSEL WRAPPER -->
	<div class="float_left" id="carousel_wrapper">
		<div class="carousel_shadow">
			<div id="carousel">
				<?php // HOW MANY PRIZES IN THE SPINNER WHEEL
					$cnt = $vPrizeCnt;
					$item_cnt = ($cnt - 1);
					$loopCnt = $vPrizeCnt * 8;
					for($i=1; $i<=$loopCnt; $i++){
						for($p=0; $p<=$item_cnt; $p++){
							echo "<img src='spin2win/_img/style$style/prize_$p.png' class='prize_img float_left' />";
						}
					}
				?>
				<div class="clear_fix"></div>
			</div>
		</div>
	</div>
	
	<!-- LEDGEND WRAPPER -->
	<div class="float_left" id="legend_wrapper">
		<!-- LEDGEND RIGHT SIDE -->
		<div class="float_left align_center" id="legend_right">
			<div class="float_left align_center" id="top_legend">AVAILABLE PRIZES</div>
		
			<div class="float_left" id="pointer_wrapper">
				<div class="pointer_down float_right"></div>
				</div>
				<div class="float_left" id="legend_text">
					<ul class="legend_list align_left">
					<?php // PRIZES AVAILABLE
						foreach($aPrizes as $prize){
							echo "<li>$prize</li>";
						}
					?>
					</ul>
			</div>
		</div>
	</div>	

	<input type="hidden" id="prize-id" name="prize-id" value="">
	<input type="hidden" id="prize-name" name="prize-name" value="">

</div>