<?php
	session_start();
	//ob_start();
	//error_reporting(7);
	include('db.php');
	include('fn.php');
	fnEstablishGeneralConnection();
?>

<!DOCTYPE html>
<html lang="en">
  <head> <?php include('_inc/incHead.php'); ?> </head>

  <body class="kiosk style<?php echo $params['style'] ?>">

    <?php include('_inc/incHeader.php'); ?>

	<?php include('_inc/incBody.php'); ?>
    
    <?php include('_inc/incFooter.php'); ?>

  </body>

<?php fnCloseGeneralConnection(); ?>
</html>
<?php //ob_flush(); ?>

<?php include('js/loadjs.php'); ?>